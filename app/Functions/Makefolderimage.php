<?php namespace App\Functions;

use Carbon\Carbon;

class Makefolderimage 
{
	public function folder($path = 'images/posts')
	{
		$now = Carbon::now();
		$year = $path.'/'.$now->year;
		$month = $year.'/'.$now->month;
		if(!is_dir($year)){
			mkdir($year);
		}
		if(!is_dir($month)){
			mkdir($month);
		}
        $imagePath = $path.'/'.$now->year.'/'.$now->month;

        return $imagePath;
	}
}