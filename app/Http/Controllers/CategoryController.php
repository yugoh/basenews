<?php

namespace App\Http\Controllers;

use DB;
use App\Role;
use App\Permission;
use App\Category;
use Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    private function updatePermission($title, $category)
    {
        $name = 'category_post_'.$title;
        $display = $title;
        $description = 'Hak akses untuk menggunakan kategori '.$display.' bagi user.';

        $permission = Permission::where('name','like','%category_post%')->where('display_name', $category->title)->first();

        if(!empty($permission)){
            if($category->title != $title) {
                $permission->update(['name'=>$name,'display_name'=>$display,'description'=>$description]);
            }
        }else {
            Permission::create(['name'=>$name,'display_name'=>$display,'description'=>$description]);
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::whereNull('parent_id')->with('childs','parent','childs.childs','childs.childs.childs')->orderBy('title', 'asc')->get();
        
        return view('admin.categories.index', compact('categories'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|min:3|max:255|unique:categories,title',
            'kategori' => 'numeric',
            'deskripsi' => 'max:1000'
        ]);
        
        $input['title'] = $request->input('nama');
        $input['slug'] = $request->input('nama');
        $input['description'] = $request->input('deskripsi');
        $input['meta_keyword'] = $request->input('meta_keyword');
        $input['meta_description'] = $request->input('meta_description');
        $parent_id = $request->input('kategori');
        if($parent_id != '') {
            $parent = Category::findOrFail($parent_id);
        }else {
            $input['parent_id'] = null;
        }

        $category = Category::create($input);

        if($parent_id != '') {
            $parent->childs()->save($category);
        }
        $name = 'category_post_'.$category->title;
        $display = $category->title;
        $description = 'Hak akses untuk menggunakan kategori '.$display.' bagi user.';
        Permission::create(['name'=>$name,'display_name'=>$display,'description'=>$description]);

        flash()->success('Kategori '.$category->title.' ditambahkan.');

        return Redirect::route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        if (empty($category)) {
            abort(404);
        }
        
        $categories = Category::with('childs','parent','childs.childs','childs.childs.childs')->orderBy('title', 'asc')->get();
        return view('admin.categories.edit', compact('category', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required|min:3|max:255|unique:categories,title,'.$id,
            'kategori' => 'numeric',
            'deskripsi' => 'max:1000'
        ]);
        
        $input['title'] = $request->input('nama');
        $input['slug'] = $request->input('nama');
        $input['description'] = $request->input('deskripsi');
        $input['meta_keyword'] = $request->input('meta_keyword');
        $input['meta_description'] = $request->input('meta_description');
        $parent_id = $request->input('kategori');
        
        if($parent_id != '' || $parent_id != null) {
            $parent = Category::findOrFail($parent_id);
        }
        else{
            $input['parent_id'] = null;
        }
        $category1 = Category::findOrFail($id);
        if(!$category1){
            abort(404);
        }

        if($parent_id != '' && $category1->parent_id != $parent_id) {

            $categoryChild = Category::whereId($id)->has('childs', 0)->first();
            
            if($categoryChild != null) {
                $this->updatePermission($input['title'], $categoryChild);
                $categoryChild->update($input);

                if($parent_id != '') {
                    $parent->childs()->save($categoryChild);
                }

                flash()->success('Anda telah mengedit Kategori '.$categoryChild->title);

                return Redirect::route('categories.index');
            }
            else {
                flash()->warning('Induk kategori ini tidak dapat diubah karena memiliki anak kategori!');

                return Redirect::route('categories.index');
            }
        }
        else {
            $category = Category::find($id);

            if($category->count() != 0 ){
                $this->updatePermission($input['title'], $category);
                $category->update($input);

                if($parent_id != '') {
                    $parent->childs()->save($category);
                }

                flash()->success('Kategori '.$category->title.' telah diupdate.');
            }
        }

        return Redirect::route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::whereId($id)->first();
        
        if (empty($category)) {
            abort(404);
        }

        $category_name = $category->title;
        DB::table('permissions')->where('display_name', $category->title)->delete();

        if($category->id != 1) {

            $category->delete();

            flash()->warning('Kategori '.$category_name.' telah dihapus.');

        }else {
            flash()->warning('Kategori ini tidak dapat dihapus karena merupakan kategori Uncategory');
        }
        
        return Redirect::route('categories.index');
    }
}
