<?php

namespace App\Http\Controllers;

use Redirect;
use App\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:manag-contacts');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $contacts = Contact::orderBy('id', 'desc')->paginate(10);
        return view('admin.contacts.index', compact('contacts'))->with('i', ($request->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact = Contact::find($id);
        if(!$contact){
            abort(404);
        }
        $contact->status = 0;
        $contact->save();

        return view('admin.contacts.show', compact('contact'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Contact::find($id);
        if(empty($contact)) {
            abort(404);
        }
        $name = $contact->name;
        $email = $contact->email;
        if(!$contact){
            return abort(404);
        }
        $contact->delete();

        flash()->warning('Anda telah menghapus Pesan dari '.$name.' ('.$email.')');

        return Redirect::route('contacts.index');
    }

    public function search(Request $request)
    {
        $this->validate($request, [
            's' => 'required|min:2|max:150'
        ]);

        $s = $request->input('s');

        $contacts = Contact::where('name', 'like', '%'.$s.'%')->orWhere('email', 'like', '%'.$s.'%')->orderBy('name', 'asc')->paginate(10);

        return view('admin.contacts.index', compact('contacts', 's'))->with('i', ($request->input('page', 1) -1) * 1);
    }
}
