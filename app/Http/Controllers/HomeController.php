<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Post;
use App\User;
use App\Banner;
use App\Contact;
use App\Functions\Popularity;
use Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'period' => 'in:one_day_stats,seven_days_stats,thirty_days_stats,all_time_stats',
        ]);
        $publish = Post::withDrafts()->where('published', 1)->count();
        $draft = Post::withDrafts()->where('published', 0)->count();
        $countuser = User::count();
        //$countbanner = Banner::count();
        $countcontact = Contact::where('status', 1)->count();

        if(Auth::user()->can('manag-posts')) {
            if(Auth::user()->can('list-posts')) {
                $posts = Post::withDrafts()->with('user')->orderBy('id', 'desc')->take(5)->get();
            }else {
                $posts = Post::withDrafts()->with('user')->where('user_id', Auth::user()->id)->orderBy('id', 'desc')->take(5)->get();
            }
        }
        
        
        //$banners = Banner::withDrafts()->orderBy('id', 'desc')->take(5)->get();
        $contacts = Contact::orderBy('id', 'desc')->take(5)->get();

        if($request->has('period')){
            $period = $request->get('period');
        }else {
            $period = 'one_day_stats';
        }
        $popularity = new Popularity();
        $item = $popularity->getStats($period, 'DESC', 'App\Post')->take(10)->get();
        
        return view('admin.index', compact('publish','draft','countuser','countbanner','countcontact','posts','banners','contacts','item','period'));
    }
}
