<?php

namespace App\Http\Controllers;

use Redirect;
use App\Label;
use Illuminate\Http\Request;

class LabelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $labels = Label::orderBy('title', 'asc')->paginate(20);
        return view('admin.labels.index', compact('labels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|min:3|max:255|unique:labels,title',
            'deskripsi' => 'max:1000'
        ]);
        $input = $request->only('meta_keyword', 'meta_description');
        $input['title'] = $request->input('nama');
        $input['slug'] = $request->input('nama');
        $input['description'] = $request->input('deskripsi');

        $label = Label::create($input);

        flash()->success('Label ditambahkan.');

        return Redirect::route('labels.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $label = Label::findOrFail($id);
        if (empty($label)) {
            abort(404);
        }
        return view('admin.labels.edit', compact('label'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required|min:3|max:255|unique:labels,title,'.$id,
            'deskripsi' => 'max:1000',
            'meta_keyword' => 'max:250',
            'meta_description' => 'max:1000'
        ]);
        $input = $request->only('meta_keyword', 'meta_description');
        $input['title'] = $request->input('nama');
        $input['slug'] = $request->input('nama');
        $input['description'] = $request->input('deskripsi');

        $label = Label::findOrFail($id);
        if (empty($label)) {
            abort(404);
        }
        $label->update($input);

        flash()->success($label->title.' diedit.');

        return Redirect::route('labels.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $label = Label::findOrFail($id); 
        if (empty($label)) {
            abort(404);
        }
        $label->delete();

        flash()->warning($label->title.' dihapus!');

        return Redirect::route('labels.index');
    }
}
