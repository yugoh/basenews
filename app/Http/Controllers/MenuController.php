<?php

namespace App\Http\Controllers;

use DB;
use App\Page;
use App\Menu;
use Redirect;
use Validator;
use App\Category;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:manag-menus');
    }

    public function top()
    {
        $menus = Menu::where('type', 'top')->orderBy('order', 'asc')->get();
        $pages = Page::all();
        $categories = Category::whereNull('parent_id')->with('childs','parent','childs.childs','childs.childs.childs')->orderBy('title', 'asc')->get();
        return view('admin.menus.top', compact('menus', 'pages', 'categories'));
    }

    public function updateTop(Request $request)
    {
        $count = count($request->input('id'));

        $n = 1;
        for($i=0;$i<=$count-1;$i++) {
            $menu = Menu::find($request->input('id')[$i]);
            $input['title'] = $request->input('nama')[$i];
            $input['type'] = 'top';
            $input['order'] = $n;
            $input['parent_id'] = 0;
            $input['format'] = $request->input('format')[$i];
            $input['title_attr'] = $request->input('titleattribute')[$i];
            $input['url'] = $request->input('url')[$i];
            $menu->update($input);
            $n++;
        }

        flash()->success('Menu telah diedit.');

        return Redirect::back();
    }

    private function typeMenu($type)
    {
        if( $type == 'top' ) {
            $count = DB::table('menus')->where('type', 'top')->max('order');
        }elseif($type == 'second') {
            $count = DB::table('menus')->where('type', 'second')->max('order');
        }
        return $count;
    }

    private function createMenu($input)
    {
        $menu = Menu::create($input);
        $var = [];
        $var['id'] = $menu->id;
        $var['title'] = $menu->title;
        $var['url'] = $menu->url;
        $var['format'] = $menu->format;
        $var['title_attr'] = $menu->title_attr;

        return $var;
    }

    public function ajaxPage(Request $request)
    {
        if($request->ajax()){
            
            $type = $request->input('type');
            $response = [];

            if($request->has('home')) {
                $count = $this->typeMenu($type);
                $input['title'] = 'Home';
                $input['url'] = '/';
                $input['type'] = $type;
                $input['order'] = $count + 1;
                $input['parent_id'] = 0;
                $input['format'] = 'page';
                $input['title_attr'] = 'Home';
                $response[] = $this->createMenu($input);
            }
            if($request->has('contact')) {
                $urlcontact = $request->input('contact');
                $count = $this->typeMenu($type);
                $input['title'] = 'Contact Us';
                $input['url'] = $urlcontact;
                $input['type'] = $type;
                $input['order'] = $count + 1;
                $input['parent_id'] = 0;
                $input['format'] = 'page';
                $input['title_attr'] = 'Contact Us';
                $response[] = $this->createMenu($input);
            }
            
            if($request->has('page')) {
                $page = $request->input('page');
                $count = $this->typeMenu($type);
                $c = $count + 1;
                foreach ($page as $key => $value) {
                    $page = Page::find($value['value']);
                    $input['title'] = $page->title;
                    $input['url'] = 'pages/'.$page->slug;
                    $input['type'] = $type;
                    $input['order'] = $c;
                    $input['parent_id'] = 0;
                    $input['format'] = 'page';
                    $input['title_attr'] = $page->title;
                    $response[] = $this->createMenu($input);
                    $c++;
                }
            }
        }

        return response()->json($response);
    }

    public function ajaxLink(Request $request)
    {
        if($request->ajax()){
            $type = $request->input('type');
            $response = [];

            if($request->has('url')) {
                $url = $request->input('url');
                $name = $request->input('name');
                $count = $this->typeMenu($type);
                $var = [];
                $input['title'] = $name;
                $input['url'] = $url;
                $input['type'] = $type;
                $input['order'] = $count + 1;
                $input['parent_id'] = 0;
                $input['format'] = 'custom';
                $input['title_attr'] = $name;
                $response[] = $this->createMenu($input);

                return response()->json($response);
            }
        }
    }

    public function ajaxCategory(Request $request)
    {
        if($request->ajax()){
            $type = $request->input('type');
            $category = $request->input('category');

            if($request->has('category')) {
                $category = $request->input('category');
                $count = $this->typeMenu($type);
                $c = $count + 1;
                foreach ($category as $key => $value) {
                    $var = [];
                    $category = Category::find($value['value']);
                    $input['title'] = $category->title;
                    $input['url'] = 'kategori/'.$category->slug;
                    $input['type'] = $type;
                    $input['order'] = $c;
                    $input['parent_id'] = 0;
                    $input['format'] = 'category';
                    $input['title_attr'] = $category->title;
                    $response[] = $this->createMenu($input);
                    $c++;
                }
            }

            return response()->json($response);
        }
    }

    public function delete(Request $request)
    {
        if($request->ajax()){
            $menu = Menu::findOrFail($request->input('id'));
            $menu->delete();

            $response = ['result'=>'success','id'=>$menu->id];

            return response()->json($response);
        }
    }
}
