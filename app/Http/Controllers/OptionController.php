<?php

namespace App\Http\Controllers;

use Redirect;
use App\Option;
use Illuminate\Http\Request;

class OptionController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:manag-settings');
    }

    public function setGlobal()
    {
    	$options = Option::all();
    	return view('admin.options.settings', compact('options'));
    }

    public function restoreGlobal(Request $request)
    {
    	$input = $request->all();
        $i = 0;
        foreach($input as $key => $value){
        	if($i > 0){
        		$set = Option::where('key', $key)->first();
	        	$inp['value'] = $value;
	        	$set->update($inp);
        	}
        	$i++;
        }

        flash()->success('Anda berhasil memperbarui Pengaturan Umum.');

        return Redirect::back();
    }

    public function restoreMail(Request $request)
    {
    	 $serialize = [1 => $request->input('email1'), 2=>$request->input('email2'), 3=>$request->input('email3')];
        $setting = Option::where('key', 'email_receiver')->first();
        if (empty($setting)) {
            abort(404);
        }
        $serial['value'] = serialize($serialize);
        $setting->update($serial);
        
        flash()->success('Anda berhasil memperbarui Pengaturan Penerima Email.');

        return Redirect::back();
    }
}
