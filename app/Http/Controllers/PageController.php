<?php
namespace App\Http\Controllers;

use App\Page;
use Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pages = Page::withDrafts()->paginate(10);
        return view('admin.pages.index', compact('pages'))->with('i', ($request->input('page', 1) -1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required|min:3|max:150|unique:pages,title',
            'publish' => 'boolean'
        ]);
        $title = trim(strip_tags($request->input('judul')));
        $input['title'] = $title;
        $input['slug'] = $title;
        $description = trim($request->input('deskripsi'));
        $input['description'] = $description;
        $input['meta_description'] = trim(strip_tags($request->input('meta_description')));
        $input['meta_keyword'] = trim(strip_tags($request->input('meta_keyword')));
        $input['published'] = $request->input('publish');
        $input['user_id'] = Auth::user()->id;
        
        $page = Page::create($input);

        flash()->success('Halaman '.$page->title);
        
        return Redirect::route('pages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::withDrafts()->whereId($id)->first();
        if(!$page){
            abort(404);
        }
        return view('admin.pages.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'judul' => 'required|min:3|max:150|unique:pages,title,'.$id,
            'publish' => 'boolean'
        ]);
        $page = Page::withDrafts()->whereId($id)->first();
        if(!$page){
            abort(404);
        }
        $input['title'] = $request->input('judul');
        $input['slug'] = $request->input('judul');
        $input['description'] = $request->input('deskripsi');
        $input['meta_description'] = trim(strip_tags($request->input('meta_description')));
        $input['meta_keyword'] = trim(strip_tags($request->input('meta_keyword')));
        $input['published'] = $request->input('publish');
        
        $page->update($input);

        flash()->success($page->title. ' diupdate.');
        
        return Redirect::route('pages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::withDrafts()->whereId($id)->first();
        if(!$page) {
            abort(404);
        }
        $page->delete();

        flash()->warning($page->title.' dihapus!');

        return Redirect::route('pages.index');
    }

    public function search(Request $request)
    {
        $this->validate($request, [
            's' => 'required|min:3|max:150'
        ]);

        $s = $request->input('s');

        $pages = Page::withDrafts()->with('user')->where('title', 'like', '%'.$s.'%')->orderBy('title', 'desc')->paginate(10);

        return view('admin.pages.index', compact('pages', 's'))->with('i', ($request->input('page', 1) - 1) * 10);
    }
}
