<?php

namespace App\Http\Controllers;

use DB;
use File;
use Redirect;
use App\Post;
use App\Label;
use App\Category;
use App\Role;
use Validator;
use App\Permission;
use Illuminate\Http\Request;
use App\Functions\Makefolderimage;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class PostController extends Controller
{
    private $size = [[50, 50], [80, 80], [420, 210]];

    private function addImage($image)
    {
        $extension = pathinfo($image, PATHINFO_EXTENSION);
        $name = rand(1111111111,9999999999).'.'.$extension;
        $makeFolder = new Makefolderimage();
        $imagePath = $makeFolder->folder();
        $size = $this->size;
        for($i=0; $i<=count($size) - 1; $i++){
            $img = Image::make($image);
            //dd($img);

            // mengambil array ukuran dari properti $size
            $s = $size[$i];

            $img->fit($s[0], $s[1]);
            //$img->fit(400, 200);

            $destination = $imagePath.'/'.$s[0].'x'.$s[1].'_'.$name;

            $img->save($destination);
        }

        return $name; 
    }

    private function delImage(Post $post) 
    {
        $makeFolder = new Makefolderimage();
        $imagePath = $makeFolder->folder();
        $size = $this->size;
        for($i=0; $i<=count($size) - 1; $i++) {

            // mengambil array ukuran dari properti $size
            $s = $size[$i];

            $destination = $imagePath.'/'.$s[0].'x'.$s[1].'_'.$post->image;

            File::delete($destination);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles_user = Auth::user();
        if($roles_user->can('list-posts')){
            $posts = Post::withDrafts()->with('category', 'user')->orderBy('id', 'desc')->paginate(10);
        }else {
            $posts = Post::withDrafts()->with('category', 'user')->where('user_id', $roles_user->id)->orderBy('id', 'desc')->paginate(10);
        }
        return view('admin.posts.index', compact('posts'))->with('i', ($request->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles_user = Auth::user();
        $role_id = [];
        foreach($roles_user->roles as $role){
            $role_id[] = $role;
        }
        $rolePermissions = [];
        foreach($role_id as $role) {
            foreach($role->catperm as $permission) {
                $rolePermissions[] = $permission->display_name;
            }
        }
        //dd($rolePermissions);
        $categories_perm = [];
        foreach ($rolePermissions as $key) {
            $cat = Category::where('title', $key)->first();
            $categories_perm[] = $cat->id;
        }
        if($roles_user->hasRole('owner') || $roles_user->can('use-all-categories')){
             $categories = Category::whereNull('parent_id')->with('childs','parent','childs.childs','childs.childs.childs')->orderBy('title', 'asc')->get();
        }else {
            $categories = Category::whereIn('id', $categories_perm)->orderBy('title', 'asc')->get();
        }
        $labels = Label::orderBy('title', 'asc')->get();
       
        return view('admin.posts.create', compact('categories', 'categories_perm', 'labels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required|min:3|max:150|unique:posts,title',
            'publish' => 'boolean'
        ]);
        
        $title = trim(strip_tags($request->input('judul')));
        $input['title'] = $title;
        $input['slug'] = $title;
        $description = trim($request->input('deskripsi'));
        $input['description'] = $description;
        
        $input['meta_description'] = $request->input('metadescription');
        $input['meta_keyword'] = $request->input('metakeyword');
        $input['published'] = $request->input('publish');
        
        if($request->has('image')){
            $image = $request->input('image');
           
            $name = $this->addImage($image);
                
            $input['image'] = $name;
        }
        
        $input['user_id'] = Auth::user()->id;

        $post = Post::create($input);
        
        if($request->has('category')){
            $category = $request->input('category');
            $post->category()->attach($category);
        }
        if($request->has('label')){
            $label = $request->input('label');
            $post->label()->attach($label);
        }

        flash()->success($post->title.' ditambahkan.');
        
        return Redirect::route('posts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles_user = Auth::user();
        if($roles_user->can('edit-posts')){
            $post = Post::withDrafts()->with('category','label')->whereId($id)->first();
        }elseif($roles_user->can('manag-posts')) {
            $post = Post::withDrafts()->with('category','label')->where('user_id', $roles_user->id)->whereId($id)->first();
        }else {
            return Redirect::route('dashboard');
        }
        
        if(!$post) {
            abort(404);
        }
        $role_id = [];
        foreach($roles_user->roles as $role){
            $role_id[] = $role;
        }
        $rolePermissions = [];
        foreach($role_id as $role) {
            foreach($role->catperm as $permission) {
                $rolePermissions[] = $permission->display_name;
            }
        }
        $categories_perm = [];
        foreach ($rolePermissions as $key) {
            $cat = Category::where('title', $key)->first();
            $categories_perm[] = $cat->id;
        }
        if($roles_user->hasRole('owner') || $roles_user->can('use-all-categories')){
             $categories = Category::whereNull('parent_id')->with('childs','parent','childs.childs','childs.childs.childs')->orderBy('title', 'asc')->get();
        }else {
            $categories = Category::whereIn('id', $categories_perm)->orderBy('title', 'asc')->get();
        }
        $labels = Label::orderBy('title', 'asc')->get();

        return view('admin.posts.edit', compact('post', 'categories', 'categories_perm', 'labels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'judul' => 'required|min:3|max:150|unique:posts,title,'.$id,
            'publish' => 'boolean'
        ]);
        $roles_user = Auth::user();
        if($roles_user->can('edit-posts')){
            $post = Post::withDrafts()->whereId($id)->first();
        }elseif($roles_user->can('manag-posts')) {
            $post = Post::withDrafts()->where('user_id', $roles_user->id)->whereId($id)->first();
        }else {
            return Redirect::route('dashboard');
        }
        if(!$post) {
            abort(404);
        }
        $title = trim(strip_tags($request->input('judul')));
        $input['title'] = $title;
        $input['slug'] = $title;
        $description = trim($request->input('deskripsi'));
        $input['description'] = $description;
        
        $input['meta_description'] = $request->input('metadescription');
        $input['meta_keyword'] = $request->input('metakeyword');
        $input['published'] = $request->input('publish');
        
        $image = $request->input('image');
        if($post->image != '' || $post->image != null) {

            if($image != '') {
                $baseimage  = pathinfo($image, PATHINFO_BASENAME);

                if($baseimage != $post->image){
                    // Hapus gambar, melalui fungsi delImage()
                    $this->delImage($post);
                    // Tambahkan gambar, melalui fungsi addImage()
                    $name = $this->addImage($image);
                    $saveimage = $name;
                }else {
                    $saveimage = pathinfo($image, PATHINFO_BASENAME);
                }
                
            }elseif($image == '') {
                $this->delImage($post);
                $saveimage = null;
            }

        }else {
            if($image != '') {
                $name = $this->addImage($image);
                $saveimage = $name;
            }else {
                $saveimage = null;
            }

        }

        $input['image'] = $saveimage;

        $post->update($input);
        
        if($request->has('category')){
            $category = $request->input('category');
            $post->category()->sync($category);
        }
        if($request->has('label')){
            $label = $request->input('label');
            $post->label()->sync($label);
        }

        flash()->success($post->title.' diupdate.');
        
        return Redirect::route('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::withDrafts()->whereId($id)->first();
        if (empty($post)) {
            abort(404);
        }
        $namepost = $post->title;
        $categories = $post->category;
        $labels = $post->label;
        $track = $post->stats;

        if($track != null){
            $track->delete();
        }
        
        foreach($categories as $category){
            $post->category()->detach($category->id);
        }
        /*foreach($labels as $label){
            $post->label()->detach($label->id);
        }*/

        $this->delImage($post);

        $post->delete();

        flash()->warning('Post '.$namepost.' dihapus!');

        return Redirect::route('posts.index');
    }

    public function search(Request $request)
    {
        $this->validate($request, [
            's' => 'required|min:3|max:150'
        ]);

        $s = $request->input('s');

        $roles_user = Auth::user();
        if($roles_user->can('list-posts')){
            $posts = Post::with('category','user')->where('title', 'like', '%'.$s.'%')->paginate(10);
        }else {
            $posts = Post::with('category','user')->where('title', 'like', '%'.$s.'%')->where('user_id', $roles_user->id)->paginate(10);
        }

        return view('admin.posts.index', compact('posts', 's'))->with('i', ($request->input('page', 1) - 1) * 10);
    }

    public function ajaxcategory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category' => 'required|min:3|max:50',
            'parent' => 'numeric',
            ]);

        if($validator->fails()){
            $response = array(
                'status' => 'error',
                'message' => $validator->errors()->all(),
            );
        }
        elseif($request->ajax()){
            $input['title'] = $request->input('category');
            $input['slug'] = $request->input('category');
            if($request->has('parent')){
                $input['parent_id'] = $request->input('parent');
            }
            
            $category = Category::create($input);

            $name = 'category_post_'.$category->title;
            $display = $category->title;
            $description = 'Hak akses untuk menggunakan kategori '.$display.' bagi user.';
            Permission::create(['name'=>$name,'display_name'=>$display,'description'=>$description]);

            $response = ['status'=>'success','id'=>$category->id,'name'=>$category->title];
        }
        
        return response()->json($response);
    }

    public function ajaxlabel(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'label' => 'required|min:3|max:50',
            ]);

        if($validator->fails()){
            $response = array(
                'status' => 'error',
                'message' => $validator->errors()->all(),
            );
        }
        elseif($request->ajax()){
            $input['title'] = $request->input('label');
            $input['slug'] = $request->input('label');
            $label = Label::create($input);

            $response = ['status'=>'success','id'=>$label->id,'name'=>$label->title];
        }
        
        return response()->json($response);
    }
}
