<?php

namespace App\Http\Controllers;

use DB;
use App\Role;
use App\Permission;
use Redirect;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:manag-roles');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = Role::orderBy('id', 'DESC')->paginate(5);
        return view('admin.roles.index', compact('roles'))->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3|max:255|unique:roles,name',
            'display_name' => 'required|min:3|max:255',
            'description' => 'required',
            'permission' => 'required',
        ]);
        $data = $request->only('name', 'display_name', 'description');

        $role = Role::create($data);

        foreach ($request->input('permission') as $key => $value) {
            $role->attachPermission($value);
        }

        flash()->success('Menambahkan Role '.$role->display_name.'.');

        return Redirect::route('roles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::findOrFail($id);
        if(!$role) {
            abort(404);
        }
        $rolePermissions = DB::table("permission_role")->where("permission_role.role_id",$id)
            ->pluck('permission_role.permission_id','permission_role.permission_id')->all();

        return view('admin.roles.edit', compact('role', 'rolePermissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:3|max:255|unique:roles,name,'.$id,
            'display_name' => 'required|min:3|max:255',
            'description' => 'required',
            'permission' => 'required',
        ]);
        $data = $request->only('name', 'display_name', 'description');

        $role = Role::findOrFail($id);
        if(!$role) {
            abort(404);
        }
        $role->update($data);

        DB::table("permission_role")->where("permission_role.role_id",$id)->delete();

        foreach ($request->input('permission') as $key => $value) {
            $role->attachPermission($value);
        }

        flash()->success('Role '.$role->display_name.' telah disimpan.');

        return Redirect::route('roles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role_name = $role->display_name;

        // Regular Delete
        $role->delete(); // This will work no matter what

        // Force Delete
        $role->users()->sync([]); // Delete relationship data
        $role->perms()->sync([]); // Delete relationship data

        $role->forceDelete();
        
        flash()->warning('Role '.$role_name.' telah dihapus.');
        
        return Redirect::route('roles.index');
    }
}
