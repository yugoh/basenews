<?php

namespace App\Http\Controllers;

use DB;
use Redirect;
use App\User;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::withDrafts()->orderBy('updated_at', 'DESC')->paginate(10);
        
        return view('admin.users.index', compact('users'))->with('i', ($request->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('display_name','id')->all();
        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'firstname' => 'required|min:3|max:30',
            'lastname' => 'min:3|max:255',
            'username' => 'required|min:3|max:255|unique:users,username',
            'email' => 'required|email|max:255|unique:users,email',
            'password' => 'required|min:6|confirmed',
            'suspended' => 'required|boolean',
        ]);
        $data = $request->only('firstname', 'lastname', 'username','email', 'suspended');
        $data['password'] = bcrypt($request->password);

        $user = User::create($data);

        $user_role = Auth::user();

        if($user_role->can('add-role')) {
            if(!empty($request->roles)){
                foreach($request->roles as $key => $value){
                    $user->roles()->attach($value);
                }
            }else {
                $role = Role::where('name', 'admin')->first();
                $user->roles()->attach($role->id);
            }
        }else {
            $role = Role::where('name', 'admin')->first();
            $user->roles()->attach($role->id);
        }

        flash()->success('Menambahkan user '.$user->firstname.' '.$user->lastname.'.');

        return redirect()->route('users.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::withDrafts()->whereId($id)->first();
        if(empty($user)) {
            abort(404);
        }
        $roles = Role::pluck('display_name','id')->all();
        $userRole = $user->roles->pluck('id','id')->toArray();
        return view('admin.users.edit', compact('user', 'roles','userRole'));
    }

    public function editProfile($id)
    {
        $user = User::find($id);
        if(empty($user)) {
            abort(404);
        }
        if($user->id == Auth::user()->id){
            $roles = Role::pluck('display_name','id')->all();
            $userRole = $user->roles->pluck('id','id')->toArray();
            return view('admin.users.edit', compact('user', 'roles', 'userRole'));
        }else {
            return Redirect::route('dashboard');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'firstname' => 'required|min:3|max:30',
            'lastname' => 'min:3|max:255',
            'username' => 'required|min:3|max:255|unique:users,username,'.$id,
            'email' => 'required|email|max:255|unique:users,email,'.$id,
            'suspended' => 'boolean',
        ]);

        $data = $request->only('firstname', 'lastname', 'username','email', 'suspended');

        $thisuser = Auth::user();

        $user = User::withDrafts()->whereId($id)->first();
        if(empty($user)) {
            abort(404);
        }

        if(($thisuser->can('edit-users') || $thisuser->id == $user->id) && $thisuser->suspended != 1) {
            if($user->id == 1 || !$thisuser->can('edit-users')) {
                $data['suspended'] = 0;
            }
            $user->update($data);
        }else {
            return Redirect::route('dashboard');
        }

        if($thisuser->can('add-role')) {
            DB::table('role_user')->where('user_id',$id)->delete();
            if(!empty($request->roles)){
                foreach($request->roles as $key => $value){
                    $user->roles()->attach($value);
                }
            }else {
                $role = Role::where('name', 'admin')->first();
                $user->roles()->attach($role->id);
            }
        }

        flash()->success($user->firstname.' '.$user->lastname.' disimpan.');

        if($thisuser->can('list-users')) {
            return redirect()->route('users.index');
        }else {
            return redirect()->route('dashboard');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::withDrafts()->whereId($id)->first();
        $user_name = $user->firstname.' '.$user->lastname;
        if (!$user) {
            abort(404);
        }
        DB::table('role_user')->where('user_id',$id)->delete();

        if($user->id != 1){
            $user->delete();
            flash()->warning('User '.$user_name.' dihapus.');
        }else {
            flash()->warning('User '.$user_name.' tidak dapat dihapus.');
        }
        
        return Redirect::route('users.index');
    }

    public function changepassword($id)
    {
        $user = User::withDrafts()->whereId($id)->first();
        if (!$user) {
            abort(404);
        }
        return view('admin.users.change-password', compact('user'));
    }

    public function changePasswordProfile($id)
    {
        $user = User::findOrFail($id);
        if (!$user) {
            abort(404);
        }
        if($user->id === Auth::user()->id){
            return view('admin.users.change-password', compact('user'));
        }else {
            return Redirect::route('dashboard');
        }
    }

    public function updatePassword(Request $request, $id)
    {
        $this->validate($request, [
            'password' => 'required|min:6|confirmed',
        ]);

        $data  = $request->only('password');
        
        $data['password'] = bcrypt($request->password);

        $user      = User::withDrafts()->whereId($id)->first();
        if (!$user) {
            abort(404);
        }

        $thisuser = Auth::user();

        if(($thisuser->can('delete-users') || $thisuser->id == $user->id) && $thisuser->suspended != 1) {
            $user->update($data);
        }else {
            return Redirect::route('dashboard');
        }

        flash()->success('Password '.$user->firstname.' '.$user->lastname.' diganti.');

        if($thisuser->can('list-users')) {
            return redirect()->route('users.index');
        }else {
            return redirect()->route('dashboard');
        }
    }

    public function search(Request $request)
    {
        $u = Auth::user();

        $this->validate($request, [
            's' => 'required|min:3|max:150'
        ]);

        $s = $request->input('s');
        $pecah_kata = explode(' ', $s, 2);
        if(count($pecah_kata) < 2){
            $pecah_kata = [$s, ''];
        }

        $userall = User::where('email', $s)->orWhere('firstname', 'like', '%'.$s.'%')->orWhere('lastname', 'like', '%'.$s.'%')->orWhere('username', 'like', '%'.$s.'%')->orWhere(function($q) use ($pecah_kata) {
            $q->where('firstname', 'like', '%'.$pecah_kata[0].'%')->where('lastname', 'like', '%'.$pecah_kata[1].'%');
        })->orderBy('firstname')->orderBy('lastname')->get();
        $users =  User::where('email', $s)->orWhere('firstname', 'like', '%'.$s.'%')->orWhere('lastname', 'like', '%'.$s.'%')->orWhere('username', 'like', '%'.$s.'%')->orWhere(function($q) use ($pecah_kata) {
            $q->where('firstname', 'like', '%'.$pecah_kata[0].'%')->where('lastname', 'like', '%'.$pecah_kata[1].'%');
        })->orderBy('firstname')->orderBy('lastname')->paginate(10);

        return view('admin.users.index', compact('users', 's', 'userall'))->with('i', ($request->input('page', 1) - 1) * 10);
    }
}
