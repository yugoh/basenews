<?php

namespace App\Http\ViewComposers;

use App\Contact;
use Illuminate\Contracts\View\View;

class MessageComposer
{
    
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $countcontact = Contact::where('status', 1)->count();
        $contacts = Contact::where('status', 1)->orderBy('id', 'desc')->take(10)->get();
        
        $view->with(['countnotifcontact'=>$countcontact,'notifcontacts'=>$contacts]);
    }
}