<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    Protected $fillable = ['title','parent_id','url','type','order','title_attr', 'format'];

    public function setTitleAttribute($title)
    {
    	$this->attributes['title'] = ucwords($title);
    }
}
