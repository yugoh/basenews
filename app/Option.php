<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    Protected $fillable = ['key', 'value'];
}
