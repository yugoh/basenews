<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
    protected $fillable = [
        'name', 'display_name', 'description',
    ];

    public function setDisplayNameAttribute($display_name)
    {
        $this->attributes['display_name'] = ucwords(strtolower($display_name));
    }

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = strtolower($name);
    }

}
