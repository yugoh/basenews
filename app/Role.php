<?php

namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    protected $fillable = [
        'name', 'display_name', 'description',
    ];

    public function setDisplayNameAttribute($display_name)
    {
        $this->attributes['display_name'] = ucwords(strtolower($display_name));
    }

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = strtolower($name);
    }

    public function catperm()
    {
        return $this->belongsToMany('App\Permission')->where('name', 'like', '%category_post%');
    }
}
