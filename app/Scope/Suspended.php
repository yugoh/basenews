<?php

namespace App\Scope;

trait Suspended {
	public static function bootSuspended()
	{
		static::addGlobalScope(new SuspendedScope);
	}

	public static function withDrafts()
	{
		return with(new static)->newQueryWithoutScope(new SuspendedScope);
	}
}