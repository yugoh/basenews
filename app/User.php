<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable, EntrustUserTrait, \App\Scope\Suspended;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'username', 'email', 'password', 'suspended'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setFirstnameAttribute($firstname)
    {
        $name = str_replace(' ', '', $firstname);
        $this->attributes['firstname'] = ucfirst(strtolower($name));
    }

    public function setLastnameAttribute($lastname)
    {
        $this->attributes['lastname'] = ucwords(strtolower($lastname));
    }
}
