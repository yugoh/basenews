<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Contact::create(['name'=>'Nama satu Nama satu Nama satu Nama satu', 'email'=>'test1@gmail.com', 'telp'=>'008', 'title'=>'Hanya Test Kontak 1 dan ini percobaan saja dan percobaan saja percobaan saja.', 'description'=>'Hanya test Kontak satu', 'status'=>1]);
        App\Contact::create(['name'=>'Nama Kontak Kedua', 'email'=>'test2@gmail.com', 'telp'=>'081234567891011', 'title'=>'Hanya Test Kontak 2', 'description'=>'Hanya test Kontak dua', 'status'=>1]);
    }
}
