<?php

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = ['home','about-us','contact-us'];
        $url = ['/', 'pages/about-us', 'page/contact-us'];
        $i = 0;
        $urut = 1;
        $jumlahmenu = count($menus);
        foreach ( range(1,$jumlahmenu) as $index ){
        	App\Menu::create(['title'=>$menus[$i],'parent_id'=>0,'url'=>$url[$i],'type'=>'top','order'=>$urut,'title_attr'=>$menus[$i],'format'=>'page']);
        	$i++;
            $urut ++;
        }
    }
}
