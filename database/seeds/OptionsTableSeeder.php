<?php

use App\Option;
use Illuminate\Database\Seeder;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $getmail = serialize([1 =>'yugojiro@gmail.com', 2 =>'yusuf@samarindaweb.com']);
        $options = [
        	'sitename' => 'Samarindaweb.com',
        	'slogan' => '',
        	'logo' => '',
        	'meta_keyword' => '',
        	'meta_description' => '',
        	'facebook' => '',
        	'twitter' => '',
        	'google_plus' => '',
        	'google_analytic' => '',
        	'email_receiver' => $getmail,
            'terpopuler' => 'thirty_days_stats',
        ];

        foreach($options as $key => $value){
        	App\Option::create(['key'=>$key,'value'=>$value]);
        }
    }
}
