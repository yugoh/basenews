<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $title = ['About Us'];

        $i = 0;
        $count = count($title);
        foreach(range(1,$count) as $index){
        	$desc = $faker->realText($maxNbChars = 1000);
        	$image = $faker->imageUrl($width=50,$height=50);

        	App\Page::create(['title'=>$title[$i],'slug'=>$title[$i],'description'=>$desc,'published'=>1,'user_id'=>rand(1,2)]);
        	$i++;
        }
    }
}
