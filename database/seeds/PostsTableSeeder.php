<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $kategori = ['uncategory'];

        $i = 0;
    	foreach(range(1,count($kategori)) as $index){
            $category = App\Category::create(['title'=>$kategori[$i],'slug'=>$kategori[$i],'description'=>'Ini hanya kategori contoh.']);
            $name = 'category_post_'.$category->title;
            $display = $category->title;
            $description = 'Hak akses untuk menggunakan kategori '.$display.' bagi user.';
            App\Permission::create(['name'=>$name,'display_name'=>$display,'description'=>$description]);
    		$i++;
    	}

    	foreach(range(1,3) as $index){
        	$title = $faker->realText($maxNbChars = 70);
        	$desc = $faker->realText($maxNbChars = 500);

        	$post = App\Post::create(['title'=>$title,'slug'=>$title,'description'=>$desc,'image'=>null,'published'=>1,'user_id'=>rand(1,2)]);

            $post->category()->attach(1);
        }
    }
}
