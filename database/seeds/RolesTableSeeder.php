<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
        	[
        		'name' => 'owner',
        		'display_name' => 'Owner',
        		'description' => 'User is the owner of a given project'
        	],
        	[
        		'name' => 'admin',
        		'display_name' => 'Admin',
        		'description' => 'User is allowed to manage and edit others in admin area'
        	]
        ];

        foreach ($roles as $key => $value) {
        	Role::create($value);
        }
        
        $owner = Role::find(1);
		$user = App\User::find(1);
		$user->roles()->attach($owner->id);

		$admin = Role::find(2);
		$useradmin = App\User::find(2);
		$useradmin->roles()->attach($admin->id);

		$permission = [
            // Users
        	[
        		'name' => 'list-users',
        		'display_name' => 'User Lists',
        		'description' => 'Melihat daftar list semua pengguna.'
        	],
        	[
        		'name' => 'create-users',
        		'display_name' => 'Create Users',
        		'description' => 'User dapat membuat pengguna baru.'
        	],
        	[
        		'name' => 'edit-users',
        		'display_name' => 'Edit Users',
        		'description' => 'User dapat mengedit semua pengguna.'
        	],
        	[
        		'name' => 'delete-users',
        		'display_name' => 'Delete Users',
        		'description' => 'User dapat menghapus pengguna.'
        	],
        	[
        		'name' => 'banned-users',
        		'display_name' => 'Bannned Users',
        		'description' => 'User dapat mem-banned semua pengguna.'
        	],
        	[
        		'name' => 'change-password',
        		'display_name' => 'Change Password',
        		'description' => 'User dapat mengubah seluruh password pengguna.'
        	],
            [
                'name' => 'add-role',
                'display_name' => 'Add Role',
                'description' => 'User dapat menambahkan seluruh role/hak akses pengguna.'
            ],
            /* Roles */
            [
                'name' => 'manag-roles',
                'display_name' => 'Roles Management',
                'description' => 'User dapat membuat role/hak akses dan permission. Halaman yang Anda lihat saat ini termasuk bagian dari Role Management'
            ],
            /* Categories */
            [
                'name' => 'list-categories',
                'display_name' => 'List Post Categories',
                'description' => 'User dapat melihat semua daftar categories.'
            ],
            [
                'name' => 'create-categories',
                'display_name' => 'Create Post Categories',
                'description' => 'User dapat membuat Kategori baru.'
            ],
            [
                'name' => 'edit-categories',
                'display_name' => 'Edit Post Categories',
                'description' => 'User dapat mengedit semua kategori.'
            ],
            [
                'name' => 'delete-categories',
                'display_name' => 'Delete Post Categories',
                'description' => 'User dapat menghapus kategori.'
            ],
            [
                'name' => 'use-all-categories',
                'display_name' => 'Use All Post Categories',
                'description' => 'User dapat menggunakan semua kategori pada pembuatan post.'
            ],
            // Posts
            [
                'name' => 'manag-posts',
                'display_name' => 'Posts Management',
                'description' => 'User dapat mengatur postingan. Apabila hanya ini yang di pilih, user hanya dapat melihat postingannya sendiri, tapi tidak dapat melihat postingan yang lain. Sebaiknya di check apabila me-check permission posts yang lain.'
            ],
            [
                'name' => 'list-posts',
                'display_name' => 'Posts Lists',
                'description' => 'Melihat daftar list semua postingan.'
            ],
            [
                'name' => 'create-posts',
                'display_name' => 'Create Posts',
                'description' => 'User dapat membuat postingan baru.'
            ],
            [
                'name' => 'edit-posts',
                'display_name' => 'Edit Posts',
                'description' => 'User dapat mengedit semua postingan. Tapi, apabila "Posts Lists" di atas tidak di check, user hanya dapat mengedit postingannya sendiri.'
            ],
            [
                'name' => 'delete-posts',
                'display_name' => 'Delete Posts',
                'description' => 'User dapat menghapus postingan.'
            ],
            [
                'name' => 'add-photo-posts',
                'display_name' => 'Add Photo Posts',
                'description' => 'User dapat menambahkan gambar pada postingan.'
            ],
            /* labels */
            [
                'name' => 'list-labels',
                'display_name' => 'List Post Label',
                'description' => 'User dapat melihat semua daftar label.'
            ],
            [
                'name' => 'create-labels',
                'display_name' => 'Create Post Label',
                'description' => 'User dapat membuat Label baru.'
            ],
            [
                'name' => 'edit-labels',
                'display_name' => 'Edit Post Label',
                'description' => 'User dapat mengedit semua Label.'
            ],
            [
                'name' => 'delete-labels',
                'display_name' => 'Delete Post Label',
                'description' => 'User dapat menghapus Label.'
            ],
            [
                'name' => 'use-labels',
                'display_name' => 'Use All Post Label',
                'description' => 'User dapat menggunakan Label pada pembuatan post.'
            ],
            /* pages */
            [
                'name' => 'list-pages',
                'display_name' => 'List Pages',
                'description' => 'User dapat melihat semua daftar Halaman/Pages.'
            ],
            [
                'name' => 'create-pages',
                'display_name' => 'Create Pages',
                'description' => 'User dapat membuat Halaman/Pages baru.'
            ],
            [
                'name' => 'edit-pages',
                'display_name' => 'Edit Pages',
                'description' => 'User dapat mengedit semua Halaman/Pages.'
            ],
            [
                'name' => 'delete-pages',
                'display_name' => 'Delete Pages',
                'description' => 'User dapat menghapus Halaman/Pages.'
            ],
            /* menus */
            [
                'name' => 'manag-menus',
                'display_name' => 'Management Menus',
                'description' => 'User dapat mengelola seluruh Menu.'
            ],
            /* settings/pengaturan */
            [
                'name' => 'manag-settings',
                'display_name' => 'Management Settings',
                'description' => 'User dapat mengelola seluruh Pengaturan.'
            ],
            /* contacts */
            [
                'name' => 'manag-contacts',
                'display_name' => 'Management Get Message / Contacts',
                'description' => 'User dapat mengelola seluruh Pesan yang dikirim pengunjung web melalui kontak form.'
            ],
            
        ];

        foreach ($permission as $key => $value) {
        	$perm = Permission::create($value);
        	$owner->attachPermission($perm);
        }

    }
}
