<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create(['firstname'=>'Admin', 'username'=>'admin', 'email'=>'admin@gmail.com','password'=>Hash::make('samarinda')]);
        App\User::create(['firstname'=>'Admin2', 'username'=>'admin2', 'email'=>'admin2@gmail.com','password'=>Hash::make('samarinda')]);
    }
}
