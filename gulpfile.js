const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.sourcemaps = false;

elixir(mix => {
    mix.sass('app.scss');
       //.webpack('app.js');
    mix.styles([
        '../../../public/css/app.css',
        '../../../node_modules/font-awesome/css/font-awesome.css',
    ], 'public/css/app.css')
       .styles([
        '../../../node_modules/sweetalert/dist/sweetalert.css',
        '../../../public/distLte/plugins/select2/select2.css',
    ], 'public/css/admin-app.css')
       .styles([
        '../../../public/distLte/css/AdminLTE.css',
        '../../../public/distLte/css/skins/_all-skins.css',
        '../../../public/css/style_admin.css',
    ], 'public/css/admin-style.css');
    mix.scripts([
        '../../../node_modules/jquery/dist/jquery.js',
        '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
    ], 'public/js/all.js')
       .scripts([
        '../../../node_modules/jquery/dist/jquery.js',
        '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap.js',
         '../../../public/distLte/plugins/fastclick/fastclick.js',
        '../../../public/distLte/js/app.js',
        '../../../node_modules/sweetalert/dist/sweetalert.min.js',
        '../../../public/distLte/plugins/select2/select2.js',
        '../../../public/distLte/plugins/slimScroll/jquery.slimscroll.js',
        '../../../public/distLte/plugins/sparkline/jquery.sparkline.min.js',
        '../../../public/distLte/js/demo.js',
        '../../../public/js/custom-admin.js',
    ], 'public/js/admin-all.js');
    mix.version(['css/app.css', 'css/admin-app.css', 'css/admin-style.css', 'js/admin-all.js']);
    mix.copy('node_modules/font-awesome/fonts', 'public/fonts');
    mix.copy('node_modules/font-awesome/fonts', 'public/build/fonts');
});
