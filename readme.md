## Aplikasi News Web Base

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Basenews merupakan aplikasi dasar untuk pembuatan website berita atau blog. Basenews ini menggunakan hak akses yang lengkap yang dapat disesuaikan dengan kebutuhan, baik manajemen user, post, kategori, sampai dapat memilih kategori apa saja yang dapat digunakan oleh user.

Saat dirilis aplikasi ini menggunakan framework Laravel 5.4. Dengan halaman admin menggunakan AdminLTE. 

Sudah include di dalam composer.json package yang mendukung aplikasi ini yaitu:
* barryvdh/laravel-debugbar 2.3,
* laracasts/flash 2.0,
* laravelcollective/html 5.3.0,
* intervention/image 2.3,
* cocur/slugify 2.3,
* riverskies/laravel-mobile-detect 1.0
* zizaco/entrust 5.2,
* laravel/tinker 1.0

Selain itu, untuk WYSIWYG menggunakan tinymce yang dikolaborasi dengan filemanager.

## Cara menggunakannya:

Cara menggunakannya sangat sederhana yakni:
1. Download atau gunakan git clone bagi yang menggunakan git.
2. Pada cmd, ketik 'composer update' (untuk mendownload package yang dibutuhkan).
3. Hilangkan tanda komentar untuk menggunakan package di app.php pada yang berada pada folder config.
4. Tambahkan file .env dan ganti nama dan user database sesuai database anda.
5. Ganti isi CACHE_DRIVER yang sebelumnya 'file' menjadi 'array' (tanpa tanda petik) apabila tidak ingin ada error terhadapr package zizaco/entrust.
4. Setelah membuat database, ganti nama database sesuai nama dan user database.
5. Lakukan migration dan seeder: php artisan migrate --seed
6. Apabila masih menggunakan localhost, ganti source filemanager, public->filemanager->config->config.php, pada 'upload_dir' ganti isian dengan '/(nama_folder_dihtdocs)/public/source/'.

## Laravel 

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
