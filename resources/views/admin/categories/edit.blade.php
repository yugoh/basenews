@extends('appadmin')

@section('title', 'Edit Category')

@section('content')

	<section class="content-header">
      <h1>
        Edit Kategori
      </h1>
    </section>

    <section class="content">
		{!! Form::model($category, ['route' => ['categories.update', $category->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'patch']) !!}
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Kategori</h3>
            </div>
				
				<div class="box-body">
					<div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
			            <label for="nama" class="col-lg-2 control-label">Nama</label>
			            <div class="col-lg-10">
			            <input class="form-control" placeholder="Nama" name="nama" type="text" value="{{$category->title}}" required>
			            @if ($errors->has('nama'))
						    <span class="help-block">
						        <strong>{{ $errors->first('nama') }}</strong>
						    </span>
						@endif
			        	</div>
			        </div><!--form control-->

			        <div class="form-group{{ $errors->has('kategori') ? ' has-error' : '' }}">
						{!! Form::label('induk', 'Induk', array('class' => 'col-md-2 control-label')) !!}
						<div class="col-md-10">
							<select name="kategori" class="form-control select2">!!}
								<option value="">Tidak Ada</option>
								
								@foreach($categories as $child)
									@if(empty($child->parent))
										<?php $id = []; 
											foreach($child->childs as $c){
												$id[] = $c->id;
											}
										?>
										@if($child->id == $category->id)
											<option value="{{$child->id}}" disabled>{{$child->title}}</option>
										@elseif(in_array($category->id, $id))
											<option value="{{$child->id}}" selected="selected">{{$child->title}}</option>
										@else
											<option value="{{$child->id}}">{{$child->title}}</option>
										@endif

										@foreach($child->childs as $child2)
											<?php $id = []; 
												foreach($child2->childs as $c){
													$id[] = $c->id;
												}
											?>
											@if($child2->id == $category->id))
												<option class="optionChild" value="{{$child2->id}}" disabled>{{$child2->title}}</option>
											@elseif(in_array($category->id, $id))
												<option class="optionChild" value="{{$child2->id}}" selected="selected">{{$child2->title}}</option>
											@else
												<option class="optionChild" value="{{$child2->id}}">{{$child2->title}}</option>
											@endif

											@foreach($child2->childs as $child)
												<?php $id = []; 
													foreach($child->childs as $c){
														$id[] = $c->id;
													}
												?>
												@if($child->id == $category->id)
												<option class="optionChildtwo" value="{{$child->id}}" disabled>{{$child->title}}</option>
												@elseif(in_array($category->id, $id))
													<option class="optionChildtwo" value="{{$child->id}}" selected="selected">{{$child->title}}</option>
												@else
												<option class="optionChildtwo" value="{{$child->id}}">{{$child->title}}</option>
												@endif
												
												@foreach($child->childs as $child3)
													<option class="optionChildthree" disabled="disabled">{{$child3->title}}</option>
												@endforeach
														
											@endforeach
										@endforeach
									@endif
								@endforeach
							</select>
							@if ($errors->has('category'))
							    <span class="help-block">
							        <strong>{{ $errors->first('category') }}</strong>
							    </span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
			            <label for="deskripsi" class="col-lg-2 control-label">Deskripsi</label>
			            <div class="col-lg-10">
						{!! Form::textarea('deskripsi', $category->description, array('class' => 'form-control', ' rows'=>5)) !!}
			                @if ($errors->has('deskripsi'))
						        <span class="help-block">
						            <strong>{{ $errors->first('deskripsi') }}</strong>
						        </span>
						    @endif
			            </div>
			        </div>

			        <div class="form-group{{ $errors->has('meta_keyword') ? ' has-error' : '' }}">
			            <label for="meta_keyword" class="col-lg-2 control-label">Meta Keyword</label>
			            <div class="col-lg-10">
			            <input class="form-control" placeholder="Meta Keyword" name="meta_keyword" type="text" value="{{$category->meta_keyword}}">
			            @if ($errors->has('meta_keyword'))
						    <span class="help-block">
						        <strong>{{ $errors->first('meta_keyword') }}</strong>
						    </span>
						@endif
			        	</div>
			        </div>

			        <div class="form-group{{ $errors->has('meta_description') ? ' has-error' : '' }}">
			            <label for="meta_description" class="col-lg-2 control-label">Meta Deskripsi</label>
			            <div class="col-lg-10">
						{!! Form::textarea('meta_description', $category->meta_description, array('class' => 'form-control', ' rows'=>5)) !!}
			                @if ($errors->has('meta_description'))
						        <span class="help-block">
						            <strong>{{ $errors->first('meta_description') }}</strong>
						        </span>
						    @endif
			            </div>
			        </div>

				</div>
				<div class="box-footer">
					<div class="pull-left">
	                    <a href="{{ url('sw-admin/categories') }}" class="btn btn-warning">Batal</a>
	                </div>
	                <div class="pull-right">
	                    <button type="submit" class="btn btn-primary">Update Kategori</button>
	                </div>
	                <div class="clearfix"></div>
		          	
		        </div>
            
        </div>
        {!! Form::close() !!}
    </section>

@endsection