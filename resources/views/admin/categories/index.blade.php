@extends('appadmin')

@section('title', 'Categories')

@section('content')

	<section class="content-header">
      <h1>
        Manajemen Kategori
      </h1>
    </section>

    <section class="content">
    	<div class="row">
    		@if (Session::has('flash_notification.message'))
				<div class="col-sm-12">
	                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
	                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                        {{ Session::get('flash_notification.message') }}
	                </div>
                </div>
            @endif
    		@permission('create-categories')
    		<div class="col-xs-12 col-md-5">
          		<div class="box">
          			<div class="box-header">
              			<h3 class="box-title">Tambah Kategori</h3>
					</div>
					
					{!! Form::open(['route' => 'categories.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
					<div class="box-body">
							<div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
			                    <label for="nama" class="col-lg-2 control-label">Nama</label>
			                    <div class="col-lg-10">
			                        <input class="form-control" placeholder="Nama" name="nama" type="text" value="{{ old('nama') }}" required>
			                         @if ($errors->has('nama'))
							            <span class="help-block">
							                <strong>{{ $errors->first('nama') }}</strong>
							            </span>
							        @endif
			                    </div>
			                </div>
			                <div class="form-group{{ $errors->has('kategori') ? ' has-error' : '' }}">
								{!! Form::label('induk', 'Induk', array('class' => 'col-md-2 control-label')) !!}
								<div class="col-md-10">
									<select name="kategori" class="form-control select2">
										<!--option value="subscriber">Subscriber</option-->
										<option value="">Tidak Ada</option>
										@foreach($categories as $category)
											@if(empty($category->parent))
												<option value="{{$category->id}}">{{$category->title}}</option>

												@foreach($category->childs as $child)
													<option class="optionChild" value="{{$child->id}}">{{$child->title}}</option>
													@foreach($child->childs as $child)
														<option class="optionChildtwo" value="{{$child->id}}">{{$child->title}}</option>
														
														@foreach($child->childs as $child)
															<option class="optionChildthree" disabled="disabled">{{$child->title}}</option>
														@endforeach
														
													@endforeach
												@endforeach
											@endif
										@endforeach
									</select>
									@if ($errors->has('category'))
							            <span class="help-block">
							                <strong>{{ $errors->first('category') }}</strong>
							            </span>
							        @endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
			                    <label for="deskripsi" class="col-lg-2 control-label">Deskripsi</label>
			                    <div class="col-lg-10">
									{!! Form::textarea('deskripsi', old('deskripsi'), array('class' => 'form-control', ' rows'=>3)) !!}
			                         @if ($errors->has('deskripsi'))
							            <span class="help-block">
							                <strong>{{ $errors->first('deskripsi') }}</strong>
							            </span>
							        @endif
			                    </div>
			                </div>
			                <div class="form-group{{ $errors->has('meta_keyword') ? ' has-error' : '' }}">
			                    <label for="meta_keyword" class="col-lg-2 control-label">Meta Keyword</label>
			                    <div class="col-lg-10">
			                        <input class="form-control" placeholder="Meta Keyword" name="meta_keyword" type="text" value="{{ old('meta_keyword') }}">
			                         @if ($errors->has('meta_keyword'))
							            <span class="help-block">
							                <strong>{{ $errors->first('meta_keyword') }}</strong>
							            </span>
							        @endif
			                    </div>
			                </div>
			                <div class="form-group{{ $errors->has('meta_description') ? ' has-error' : '' }}">
			                    <label for="meta_description" class="col-lg-2 control-label">Meta Deskripsi</label>
			                    <div class="col-lg-10">
									{!! Form::textarea('meta_description', old('meta_description'), array('class' => 'form-control', ' rows'=>3)) !!}
			                         @if ($errors->has('meta_description'))
							            <span class="help-block">
							                <strong>{{ $errors->first('meta_description') }}</strong>
							            </span>
							        @endif
			                    </div>
			                </div>
						
					</div>
					<div class="box-footer">
		            	<button type="submit" class="btn btn-primary pull-right">Tambah Kategori</button>
		            </div>
		            {!! Form::close() !!}
    			</div>
    		</div>
    		@endpermission
    		<div class="col-xs-12 col-md-7">
          		<div class="box">
    				<div class="box-body table-responsive no-padding">
		              <table class="table edit">
		                <tr>
		                  <th>Nama</th>
		                  <th>Slug</th>
		                  @permission('edit-categories')
		                  <th>Action</th>
		                  @endpermission
		                </tr>
		                @foreach($categories as $category)
		                	@if(empty($category->parent))
			                <tr>
			                  <td class="category">{{$category->title}}</td>
			                  <td>{{$category->slug}}</td>
			                  <td>
			                  	@permission('edit-categories')
			                  	<a href="{{ url('sw-admin/categories/'.$category->id.'/edit') }}" class="btn btn-xs btn-primary">
					            	<i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title data-original-title="Edit Kategori"></i> Edit
					            </a>
					            @endpermission
					            @permission('delete-categories')
			                  	{!! Form::open(['method' => 'DELETE','route' => ['categories.destroy', $category->id],'style'=>'display:inline']) !!}
                                	{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs js-submit-confirm', 'data-toggle'=>'tooltip', 'data-placement'=>'top','Hapus','data-original-title'=>'Hapus']) !!}
                            	{!! Form::close() !!}
                            	@endpermission
			                  </td>
			                </tr>
			                @endif
			                	@foreach($category->childs as $child)
			                		<tr>
					                  <td><span class="optionOne">-- {{$child->title}}</span></td>
					                  <td>{{$child->slug}}</td>
					                  <td>
					                  	@permission('edit-categories')
					                  	<a href="{{ url('sw-admin/categories/'.$child->id.'/edit') }}" class="btn btn-xs btn-primary">
							            	<i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title data-original-title="Edit Kategori"></i> Edit
							            </a>
							            @endpermission
							            @permission('delete-categories')
					                  	{!! Form::open(['method' => 'DELETE','route' => ['categories.destroy', $child->id],'style'=>'display:inline']) !!}
		                                	{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs js-submit-confirm', 'data-toggle'=>'tooltip', 'data-placement'=>'top','Hapus User','data-original-title'=>'Hapus User']) !!}
		                            	{!! Form::close() !!}
		                            	@endpermission
					                  </td>
					                </tr>
					                @foreach($child->childs as $child)
				                		<tr>
						                  <td><span class="optionTwo">---- {{$child->title}}</span></td>
						                  <td>{{$child->slug}}</td>
						                  <td>
						                  	@permission('edit-categories')
						                  	<a href="{{ url('sw-admin/categories/'.$child->id.'/edit') }}" class="btn btn-xs btn-primary">
								            	<i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title data-original-title="Edit Kategori"></i> Edit
								            </a>
								            @endpermission
								            @permission('delete-categories')
						                  	{!! Form::open(['method' => 'DELETE','route' => ['categories.destroy', $child->id],'style'=>'display:inline']) !!}
			                                	{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs js-submit-confirm', 'data-toggle'=>'tooltip', 'data-placement'=>'top','Hapus User','data-original-title'=>'Hapus User']) !!}
			                            	{!! Form::close() !!}
			                            	@endpermission
						                  </td>
						                </tr>
						                @foreach($child->childs as $child)
					                		<tr>
							                  <td><span class="optionThree">------ {{$child->title}}</span></td>
							                  <td>{{$child->slug}}</td>
							                  <td>
							                  	@permission('edit-categories')
							                  	<a href="{{ url('sw-admin/categories/'.$child->id.'/edit') }}" class="btn btn-xs btn-primary">
									            	<i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title data-original-title="Edit Kategori"></i> Edit
									            </a>
									            @endpermission
									            @permission('delete-categories')
							                  	{!! Form::open(['method' => 'DELETE','route' => ['categories.destroy', $child->id],'style'=>'display:inline']) !!}
				                                	{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs js-submit-confirm', 'data-toggle'=>'tooltip', 'data-placement'=>'top','Hapus User','data-original-title'=>'Hapus User']) !!}
				                            	{!! Form::close() !!}
				                            	@endpermission
							                  </td>
							                </tr>
					                	@endforeach
				                	@endforeach
			                	@endforeach
		                	@endforeach
		                
		              </table>
              
            		</div>
    			</div>
    		</div>
    		
    	</div>
    </section>

@endsection 