@extends('appadmin')

@section('title', 'Contacts')

@section('content')

<?php $carbon = new Carbon\Carbon; ?>

	<section class="content-header">
      	<h1>
        	Kontak Pesan
      	</h1>
  	</section>

  	<section class="content">
    	<div class="row">
        <div class="col-xs-12">
        	@if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
            @endif
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>

              <div class="box-tools">
              	{!! Form::open(['route'=>['search.contacts'],'method' => 'GET','role'=>'search']) !!}
                <div class="input-group input-group-sm" style="width: 150px;">
	                  <input type="text" name="s" class="form-control pull-right" placeholder="Search">
	                  <div class="input-group-btn">
	                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
	                  </div>
                </div>
                {!! Form::close() !!}
              </div>
            </div>

            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                	<th>No.</th>
                  <th>Pengirim</th>
                  <th>Email</th>
                  <th>telp</th>
                  <th>Judul</th>
                  <th>Tanggal Kirim</th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
                @if($contacts->count() == 0)
                	<tr><td>Tidak ada pesan.</td></tr>
                @else
	                @foreach($contacts as $contact)
	                <tr>
	                	<td>{{++$i}}</td>
	                  <td><a href="{{url('sw-admin/contacts/'.$contact->id)}}" title="Lihat Isi Pesan">{{$contact->name}}</a></td>
	                  <td><a href="{{url('sw-admin/contacts/'.$contact->id)}}" title="Lihat Isi Pesan">{{$contact->email}}</a></td>
	                  <td>{{$contact->telp}} </td>
	                  <td>{{$contact->title}} </td>
	                  <td>{{ $carbon->createFromTimeStamp(strtotime($contact->created_at))->diffForHumans() }}</td>
	                  @if($contact->status == 1)
	                  <td><span class="label label-warning">Belum Dibaca</span></td>
	                  @else
	                  <td><span class="label label-success">Terbaca</span></td>
	                  @endif
	                  <td>
		                    {!! Form::open(['method' => 'DELETE','route' => ['contacts.destroy', $contact->id],'style'=>'display:inline']) !!}
			                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs js-submit-confirm', 'data-toggle'=>'tooltip', 'data-placement'=>'top','Hapus Contact','data-original-title'=>'Hapus Contact dari '.$contact->name]) !!}
			                {!! Form::close() !!}
	                  </td>
	                </tr>
	                @endforeach
                @endif
              </table>
              <p> {{ $contacts->appends(compact('s'))->links() }} </p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>

@endsection