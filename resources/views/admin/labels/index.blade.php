@extends('appadmin')

@section('title', 'Labels')

@section('content')

	<section class="content-header">
      <h1>
        Label
      </h1>
    </section>
    
	<section class="content">
    	<div class="row">
    		@if (Session::has('flash_notification.message'))
				<div class="col-sm-12">
	                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
	                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                        {{ Session::get('flash_notification.message') }}
	                </div>
                </div>
            @endif

    		@permission('create-labels')
    		<div class="col-xs-12 col-md-5">
          		<div class="box">
          			<div class="box-header">
              			<h3 class="box-title">Tambah Label</h3>
					</div>
					
					{!! Form::open(['route' => 'labels.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
					<div class="box-body">
						<div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
			                <label for="nama" class="col-lg-2 control-label">Nama*</label>
			                <div class="col-lg-10">
			                    <input class="form-control" placeholder="Nama" name="nama" type="text" value="{{ old('nama') }}" required>
			                    @if ($errors->has('nama'))
							        <span class="help-block">
							            <strong>{{ $errors->first('nama') }}</strong>
							        </span>
							    @endif
			                </div>
			            </div>
			            <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
			                <label for="deskripsi" class="col-lg-2 control-label">Deskripsi</label>
			                <div class="col-lg-10">
								{!! Form::textarea('deskripsi', old('deskripsi'), array('class' => 'form-control', ' rows'=>5)) !!}
			                    @if ($errors->has('deskripsi'))
							        <span class="help-block">
							           <strong>{{ $errors->first('deskripsi') }}</strong>
							        </span>
							    @endif
			                </div>
			            </div>
			            <div class="form-group{{ $errors->has('meta_keyword') ? ' has-error' : '' }}">
			                    <label for="meta_keyword" class="col-lg-2 control-label">Meta Keyword</label>
			                    <div class="col-lg-10">
			                        <input class="form-control" placeholder="Meta Keyword" name="meta_keyword" type="text" value="{{ old('meta_keyword') }}">
			                         @if ($errors->has('meta_keyword'))
							            <span class="help-block">
							                <strong>{{ $errors->first('meta_keyword') }}</strong>
							            </span>
							        @endif
			                    </div>
			                </div>
			                <div class="form-group{{ $errors->has('meta_description') ? ' has-error' : '' }}">
			                    <label for="meta_description" class="col-lg-2 control-label">Meta Deskripsi</label>
			                    <div class="col-lg-10">
									{!! Form::textarea('meta_description', old('meta_description'), array('class' => 'form-control', ' rows'=>3)) !!}
			                         @if ($errors->has('meta_description'))
							            <span class="help-block">
							                <strong>{{ $errors->first('meta_description') }}</strong>
							            </span>
							        @endif
			                    </div>
			                </div>
					</div>
					<div class="box-footer">
		            	<button type="submit" class="btn btn-primary pull-right">Tambah Label</button>
		            </div>
		            {!! Form::close() !!}
		        </div>
    		</div>
    		@endpermission

    		<div class="col-xs-12 col-md-7">
          		<div class="box">
					<div class="box-body table-responsive no-padding">
		              	<table class="table edit table-hover">
		              		<tr>
		              			<th>Nama</th>
		              			<th>Slug</th>
		              			<th>Action</th>
		              		</tr>
		              		@foreach($labels as $label)
		              		<tr>
		              			<td>{{$label->title}}</td>
		              			<td>{{$label->slug}}</td>
		              			<td>
		              				@permission('edit-labels')
		              				<a href="{{ url('sw-admin/labels/'.$label->id.'/edit') }}" class="btn btn-xs btn-primary" style="margin-right:15px;">
							          	<i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title data-original-title="Edit Label"></i> Edit
							        </a>
							        @endpermission
							        @permission('delete-labels')
				                  	{!! Form::open(['method' => 'DELETE','route' => ['labels.destroy', $label->id],'style'=>'display:inline']) !!}
	                                	{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs js-submit-confirm', 'data-toggle'=>'tooltip', 'data-placement'=>'top','Hapus','data-original-title'=>'Hapus']) !!}
	                            	{!! Form::close() !!}
	                            	@endpermission
		              			</td>
		              		</tr>
		              		@endforeach
		              	</table>
		              	<p>{{$labels->render()}}</p>
					</div>
          		</div>
    		</div>
    	</div>
    </section>

@endsection