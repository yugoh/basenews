@extends('appadmin')

@section('title', 'Settings')

@section('content')

	<section class="content-header">
      <h1>
        Pengaturan
      </h1>
    </section>

	<section class="content">
		
		<div class="row">
			{!! Form::open(['route' => 'restore.global', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'files'=>'true']) !!}
			<div class="col-md-12">
				@if (Session::has('flash_notification.message'))
					<div class="alert alert-{{ Session::get('flash_notification.level') }}">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

						{{ Session::get('flash_notification.message') }}
					</div>
				@endif
				<div class="box box-success">
					<div class="box-header with-border">
		                <h3 class="box-title">Pengaturan Umum</h3>
		            </div>
					<div class="box-body">
						<div class="form-group{{ $errors->has('sitename') ? ' has-error' : '' }}">
		                    <label for="sitename" class="col-sm-2 control-label">Nama Website</label>
		                    <div class="col-sm-10">
		                        <input class="form-control" placeholder="Nama Website" name="sitename" type="text" value="{{ $options->where('key','sitename')->first()->value }}" required>
		                         @if ($errors->has('sitename'))
						            <span class="help-block">
						                <strong>{{ $errors->first('sitename') }}</strong>
						            </span>
						        @endif
		                    </div>
		                </div><!--form control-->
		                <div class="form-group{{ $errors->has('slogan') ? ' has-error' : '' }}">
		                    <label for="slogan" class="col-sm-2 control-label">Slogan</label>
		                    <div class="col-sm-10">
		                        <input class="form-control" placeholder="Slogan" name="slogan" type="text" value="{{ $options->where('key','slogan')->first()->value }}">
		                         @if ($errors->has('slogan'))
						            <span class="help-block">
						                <strong>{{ $errors->first('slogan') }}</strong>
						            </span>
						        @endif
		                    </div>
		                </div><!--form control-->
		                
		                <div class="form-group{{ $errors->has('meta_keyword') ? ' has-error' : '' }}">
		                    <label for="meta_keyword" class="col-sm-2 control-label">Meta Keyword</label>
		                    <div class="col-sm-10">
		                        <input class="form-control" placeholder="Meta Title" name="meta_keyword" type="text" value="{{ $options->where('key','meta_keyword')->first()->value }}">
		                         @if ($errors->has('meta_keyword'))
						            <span class="help-block">
						                <strong>{{ $errors->first('meta_keyword') }}</strong>
						            </span>
						        @endif
		                    </div>
		                </div><!--form control-->
		                <div class="form-group{{ $errors->has('meta_description') ? ' has-error' : '' }}">
		                    <label for="meta_description" class="col-sm-2 control-label">Meta Deskripsi</label>
		                    <div class="col-sm-10">
		                        {!! Form::textarea('meta_description', $options->where('key','meta_description')->first()->value, array('class' => 'form-control',' rows'=>3)) !!}
		                         @if ($errors->has('meta_description'))
						            <span class="help-block">
						                <strong>{{ $errors->first('meta_description') }}</strong>
						            </span>
						        @endif
		                    </div>
		                </div><!--form control-->
		                <div class="form-group{{ $errors->has('facebook') ? ' has-error' : '' }}">
		                    <label for="facebook" class="col-sm-2 control-label">Facebook</label>
		                    <div class="col-sm-10">
		                        <input class="form-control" placeholder="Nama Facebook" name="facebook" type="text" value="{{ $options->where('key','facebook')->first()->value }}">
		                         @if ($errors->has('facebook'))
						            <span class="help-block">
						                <strong>{{ $errors->first('facebook') }}</strong>
						            </span>
						        @endif
		                    </div>
		                </div><!--form control-->
		                <div class="form-group{{ $errors->has('twitter') ? ' has-error' : '' }}">
		                    <label for="twitter" class="col-sm-2 control-label">Twitter</label>
		                    <div class="col-sm-10">
		                        <input class="form-control" placeholder="Nama Twitter" name="twitter" type="text" value="{{ $options->where('key','twitter')->first()->value }}">
		                         @if ($errors->has('twitter'))
						            <span class="help-block">
						                <strong>{{ $errors->first('twitter') }}</strong>
						            </span>
						        @endif
		                    </div>
		                </div>
		                <div class="form-group{{ $errors->has('google_plus') ? ' has-error' : '' }}">
		                    <label for="google_plus" class="col-sm-2 control-label">Google Plus</label>
		                    <div class="col-sm-10">
		                        <input class="form-control" placeholder="Nama Google Plus" name="google_plus" type="text" value="{{ $options->where('key','google_plus')->first()->value }}">
		                         @if ($errors->has('google_plus'))
						            <span class="help-block">
						                <strong>{{ $errors->first('google_plus') }}</strong>
						            </span>
						        @endif
		                    </div>
		                </div>
		                <div class="form-group{{ $errors->has('google_analytic') ? ' has-error' : '' }}">
		                    <label for="google_analytic" class="col-sm-2 control-label">Google Analytic</label>
		                    <div class="col-sm-10">
		                        {!! Form::textarea('google_analytic', $options->where('key','google_analytic')->first()->value, array('class' => 'form-control',' rows'=>3)) !!}
		                         @if ($errors->has('google_analytic'))
						            <span class="help-block">
						                <strong>{{ $errors->first('google_analytic') }}</strong>
						            </span>
						        @endif
		                    </div>
		                </div><!--form control-->
		                
					</div>
					<div class="box box-info">
			            <div class="box-body">

			                <div class="pull-right">
			                    <input type="submit" class="btn btn-success" value="Simpan" />
			                </div>
			                <div class="clearfix"></div>
			            </div><!-- /.box-body -->
			        </div><!--box-->
				</div>
			</div>
			{!! Form::close() !!}

			{!! Form::open(['route' => 'restore.setmail', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
			<div class="col-md-12">
				<div class="box box-danger">
					<div class="box-header with-border">
		                <h3 class="box-title">Penerima Email dari ContactForm</h3>
		            </div>
					<div class="box-body">
		                <?php $getmail = unserialize($options->where('key','email_receiver')->first()->value) ?>
		                <div class="form-group{{ $errors->has('email1') ? ' has-error' : '' }}">
		                    <label for="email1" class="col-sm-2 control-label">Email 1</label>
		                    <div class="col-sm-10">
		                        <input class="form-control" placeholder="Email 1" name="email1" type="text" value="{{$getmail[1]}}">
		                         @if ($errors->has('email1'))
						            <span class="help-block">
						                <strong>{{ $errors->first('email1') }}</strong>
						            </span>
						        @endif
		                    </div>
		                </div><!--form control-->
		                <div class="form-group{{ $errors->has('email2') ? ' has-error' : '' }}">
		                    <label for="email2" class="col-sm-2 control-label">Email 2</label>
		                    <div class="col-sm-10">
		                        <input class="form-control" placeholder="Email 2" name="email2" type="text" value="{{$getmail[2]}}">
		                         @if ($errors->has('email2'))
						            <span class="help-block">
						                <strong>{{ $errors->first('email2') }}</strong>
						            </span>
						        @endif
		                    </div>
		                </div><!--form control-->
		                

					</div>
					<div class="box box-info">
			            <div class="box-body">
			                <div class="pull-left">
			                    <a href="{{ url('sw-admin/dashboard') }}" class="btn btn-danger">Batal</a>
			                </div>

			                <div class="pull-right">
			                    <input type="submit" class="btn btn-success" value="Simpan" />
			                </div>
			                <div class="clearfix"></div>
			            </div><!-- /.box-body -->
			        </div><!--box-->
				</div>
			</div>
			{!! Form::close() !!}

		</div>
		
	</section>



@endsection