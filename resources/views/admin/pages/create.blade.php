@extends('appadmin')

@section('title', 'Create Page')

@section('content')

@section('css')
<link rel="stylesheet" href="{{asset('distLte/plugins/iCheck/all.css')}}">
@endsection

	<section class="content-header">
      <h1>
        Tambah Halaman Baru
      </h1>
    </section>

    @if (Session::has('flash_notification.message'))
		<div class="alert alert-{{ Session::get('flash_notification.level') }}">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

			{{ Session::get('flash_notification.message') }}
		</div>
	@endif

	<section class="content">
		{!! Form::open(['route' => 'pages.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
    	<div class="row">
    		<div class="col-md-8">
				<div class="box box-success">
					<div class="box-body">
						<div class="form-group{{ $errors->has('judul') ? ' has-error' : '' }}">
			        		<div class="col-lg-12">
						        <input class="form-control input-lg" id="focus" placeholder="Masukkan judul disini" name="judul" type="text" value="{{old('judul')}}" required>
						        @if ($errors->has('judul'))
								    <span class="help-block">
								        <strong>{{ $errors->first('judul') }}</strong>
								    </span>
								@endif
					     	</div>
					    </div><!--form control-->
					    <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
			                <div class="col-lg-12">
								{!! Form::textarea('deskripsi', old('deskripsi'), array('class' => 'form-control', ' rows'=>15, 'id'=>'description')) !!}
			                    @if ($errors->has('deskripsi'))
							        <span class="help-block">
							           <strong>{{ $errors->first('deskripsi') }}</strong>
							        </span>
							    @endif
			                </div>
			            </div><!--form control-->
					</div>
					<div class="box-footer">
						<div class="pull-left">
			                <a href="{{ url('sw-admin/pages') }}" class="btn btn-warning">Batal</a>
			            </div>
			            <div class="clearfix"></div>  
				    </div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="box box-info">
					<div class="box-header with-border">
		                <h3 class="box-title">Publikasi</h3>
		            </div>
		            <div class="box-body">
		            	<div class="clearfix"></div>
		            	
		            	<div class="keterangan">
		            		<div class="col-md-12">
		            			<div class="form-group">
		            				<i class="fa fa-fire" aria-hidden="true"></i> Status: 
					                <label>
					                  <input type="radio" value="0" name="publish" class="minimal-red">
					                  Draft
					                </label>
					                <label>
					                  <input type="radio" value="1" name="publish" class="minimal-red" checked>
					                  Publikasi
					                </label>
				              	</div>
				            </div>
		            	</div>
		            </div>
		            <div class="box-footer">
						<div class="pull-left">
							
			                <a href="{{ url('sw-admin/pages/delete') }}" class="delete">Hapus Halaman ini</a>
			                
			            </div>
			            <div class="pull-right">
			                <button type="submit" class="btn btn-primary">Simpan</button>
			            </div>
			            <div class="clearfix"></div>  	
				    </div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="box box-info">
					<div class="box-header with-border">
		                <h3 class="box-title">Meta</h3>
		            </div>
		            <div class="box-body">
		            	<div class="clearfix"></div>
						
						<p>Meta Deskripsi: </p>
		            	<div class="form-group{{ $errors->has('meta_description') ? ' has-error' : '' }}">
			        		<div class="col-lg-12">
			        			{!! Form::textarea('meta_description', old('meta_description'), array('class' => 'form-control',' rows'=>3)) !!}
						        @if ($errors->has('meta_description'))
								    <span class="help-block">
								        <strong>{{ $errors->first('meta_description') }}</strong>
								    </span>
								@endif
					        </div>
					    </div><!--form control-->
					    <p>Meta Keyword: </p>
					    <div class="form-group{{ $errors->has('meta_keyword') ? ' has-error' : '' }}">
			        		<div class="col-lg-12">
						        <input class="form-control" placeholder="Meta Keyword. Contoh: berita, kaltim, dll" name="meta_keyword" type="text" value="{{old('meta_keyword')}}">
						        @if ($errors->has('meta_keyword'))
								    <span class="help-block">
								        <strong>{{ $errors->first('meta_keyword') }}</strong>
								    </span>
								@endif
					        </div>
					    </div><!--form control-->
		            </div>
		        </div>
		    </div>

    	</div>
    	{!! Form::close() !!}
	</section>

@section('js')
<script src="{{asset('distLte/plugins/iCheck/icheck.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('tinymce/tinymce.min.js') }}"></script>
<script type="text/javascript">	
tinymce.init({
   selector: '#description',
	height: 350,
	menubar:false,
	statusbar: false,

   	plugins : ["advlist autolink image charmap link", "searchreplace code", "media table contextmenu paste textcolor responsivefilemanager wordcount hr"],
   	toolbar : "undo redo | styleselect | bold italic table | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link unlink hr | image media responsivefilemanager code | forecolor backcolor code",
   
   	image_advtab: true ,
   	relative_urls: false, 

   	external_filemanager_path:"{!! str_finish(asset('filemanager'),'/') !!}",
	filemanager_title        :"Manajemen Media" , // bisa diganti terserah anda
	external_plugins         : { "filemanager" : "{{ asset('filemanager/plugin.min.js') }}"}, 
   
});
</script>
<script>
$(document).ready(function () {
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    $(".select2").select2();
});
</script>
@endsection


@endsection