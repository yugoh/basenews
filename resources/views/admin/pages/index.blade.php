@extends('appadmin')

@section('title', 'Pages')

@section('content')

@section('style')
<style>
    .table-responsive td a {
      margin-right: 15px;
    }
  </style>
@endsection

<?php  
$carbon = new Carbon\Carbon;
?>
	
	 <section class="content-header">
      	<h1>
        	Halaman
        	<small><a href="{{url('sw-admin/pages/create')}} " class="btn btn-xs btn-default">Tambah Baru</a></small>
      	</h1>
  	</section>

  	<section class="content">
    	<div class="row">
			   <div class="col-xs-12">
              @if (Session::has('flash_notification.message'))
                  <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      {{ Session::get('flash_notification.message') }}
                  </div>
              @endif
          		<div class="box">
            		<div class="box-header">
              			<h3 class="box-title"></h3>

              			<div class="box-tools">
              				{!! Form::open(['route'=>'search.pages','method' => 'GET']) !!}
                			<div class="input-group input-group-sm" style="width: 150px;">
                	
	                  			<input type="text" name="s" class="form-control pull-right" placeholder="Search">

	                  			<div class="input-group-btn">
	                    			<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
	                  			</div>
	                
                			</div>
                			{!! Form::close() !!}
              			</div>
            		</div>

               		<div class="box-body table-responsive no-padding">
               			<table class="table table-hover">
               				<tr>
               					<th>No</th>
              					<th>Judul</th>
              					<th>Penulis</th>
              					<th>Status</th>
              					<th class="hidden-xs">Dibuat</th>
								        <th>action</th>
              				</tr>
              				@foreach($pages as $page)
              				<tr>
              					<td>{{++$i}}</td>
                        @if(Auth::user()->can('edit-pages'))
              					<td><a href="{{url('sw-admin/pages/'.$page->id.'/edit')}}">{{$page->title}}</a></td>
                        @else
                        <td>{{$page->title}}</td>
                        @endif
              					<td>{{$page->user->username}}</td>
              					<td>
              						@if($page->published == 0)
		                            	<label class="label label-danger">Draft</label>
		                         	@else
		                            	<label class="label label-success">Publikasi</label>
		                          	@endif
		              			</td>
		              			<td class="hidden-xs">
              						{{ $carbon->createFromTimeStamp(strtotime($page->created_at))->diffForHumans() }}
              					</td>
              					<td>
                            @permission('edit-pages')
              						  <a href="{{ url('sw-admin/pages/'.$page->id.'/edit')}}" class="btn btn-xs btn-primary">
	                  					<i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i>
	                  				</a>
                            @endpermission
	                  				@permission('delete-pages')
			                  		{!! Form::open(['method' => 'DELETE','route' => ['pages.destroy', $page->id],'style'=>'display:inline']) !!}
			                          {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs js-submit-confirm', 'data-toggle'=>'tooltip', 'data-placement'=>'top','Hapus User','data-original-title'=>'Hapus User']) !!}
			                      {!! Form::close() !!}
		                        @endpermission
              					</td>
              				</tr>
              				@endforeach
               			</table>
               			<p>{{ $pages->appends(compact('s'))->links() }}</p>
               		</div>
            	</div>
            </div>
        </div>
    </section>

@endsection
