@extends('appadmin')

@section('title', 'Posts')

@section('content')

<?php $carbon = new Carbon\Carbon(); ?>

	<section class="content-header">
      <h1>
        Posts
        @permission('create-posts')
        <small><a href="{{url('sw-admin/posts/create')}} " class="btn btn-xs btn-default">Tambah Baru</a></small>
        @endpermission
      </h1>
  </section>

	<section class="content">
    	<div class="row">
			    <div class="col-xs-12">
              @if (Session::has('flash_notification.message'))
                  <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      {{ Session::get('flash_notification.message') }}
                  </div>
              @endif
          		<div class="box">
            		<div class="box-header">
              			<h3 class="box-title"></h3>

              			<div class="box-tools">
              				{!! Form::open(['route'=>'posts.search','method' => 'GET']) !!}
                			<div class="input-group input-group-sm" style="width: 150px;">
	                  			<input type="text" name="s" class="form-control pull-right" placeholder="Search">
	                  			<div class="input-group-btn">
	                    			<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
	                  			</div>
                			</div>
                			{!! Form::close() !!}
              			</div>
            		</div>
            		

               		<div class="box-body table-responsive no-padding">
              			<table class="table table-hover">
              				<tr>
                        <th>No</th>
              					<th>Judul</th>
              					<th>Penulis</th>
              					<th class="hidden-xs">Kategori</th>
              					<th class="hidden-xs">Status</th>
              					<th class="hidden-xs">Create</th>
								        <th>action</th>
              				</tr>
                      
              				@foreach($posts as $post)
              				<tr>
                        		<td>{{++$i}}</td>
              					<td><a href="{{url('sw-admin/posts/'.$post->id.'/edit')}}">{{$post->title}}</a></td>
              					<td>{{$post->user->firstname}} {{$post->user->lastname}}</td>
              					<td class="hidden-xs">
              						@foreach($post->category as $category)
              							<label class="label label-primary"><i class="fa fa-btn fa-tags"></i> {{$category->title}}</label>
              						@endforeach
              					</td>
              					<td class="hidden-xs">
              						@if($post->published == 0)
                            <span class="draft">Draft</span>
                          @else
                            <span class="publish">Publikasi</span>
                          @endif
              					</td>
              					<td class="hidden-xs">
              						{{ $carbon->createFromTimeStamp(strtotime($post->created_at))->diffForHumans() }}
              					</td>
              					<td>
              						  <a href="{{ url('sw-admin/posts/'.$post->id.'/edit')}}" class="btn btn-xs btn-primary" style="margin-right:15px">
	                  					<i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i>
	                  				</a>
                            @permission('delete-posts')
	                  				{!! Form::open(['method' => 'DELETE','route' => ['posts.destroy', $post->id],'style'=>'display:inline']) !!}
	                               {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs js-submit-confirm', 'data-toggle'=>'tooltip', 'data-placement'=>'top','Hapus User','data-original-title'=>'Hapus User']) !!}
	                          {!! Form::close() !!}
                            @endpermission
              					</td>
              				</tr>
              				@endforeach
              			</table>
              			<p>{{ $posts->appends(compact('s'))->links() }}</p>
              		</div>
            	</div>
            </div>
    	</div>
    </section>

@endsection