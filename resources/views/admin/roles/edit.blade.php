@extends('appadmin')

@section('title', 'Edit Role '.$role->display_name)

@section('content')

@section('css')
<link rel="stylesheet" href="{{asset('distLte/plugins/iCheck/all.css')}}">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('style')
	<style>
		.hide-list ul {
			list-style-type: none;
		}
	</style>
@endsection

	<section class="content-header">
      <h1>
        Edit Role {{$role->display_name}}
      </h1>
    </section>

    <section class="content">
		{!! Form::model($role, ['route' => ['roles.update', $role->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'patch']) !!}
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Role</h3>
        
				<div class="box-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> Ada beberapa masalah dengan inputan Anda.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
	                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	                    <label for="name" class="col-md-2 control-label">Name</label>
	                    <div class="col-md-4">
	                    	{!! Form::text('name', null, array('placeholder' => 'Nama Role','class' => 'form-control', 'required', 'autofocus')) !!}
	                         @if ($errors->has('name'))
					            <span class="help-block">
					                <strong>{{ $errors->first('name') }}</strong>
					            </span>
					        @endif
	                    </div>
	                </div>

	                <div class="form-group{{ $errors->has('display_name') ? ' has-error' : '' }}">
	                    <label for="display_name" class="col-md-2 control-label">Display Name*</label>
	                    <div class="col-md-10">
	                        {!! Form::text('display_name', null, array('placeholder' => 'Display Name','class' => 'form-control', 'required')) !!}
	                         @if ($errors->has('display_name'))
					            <span class="help-block">
					                <strong>{{ $errors->first('display_name') }}</strong>
					            </span>
					        @endif
	                    </div>
	                </div>

	                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
	                    <label for="description" class="col-md-2 control-label">Description*</label>
	                    <div class="col-md-10">
	                    	{!! Form::textarea('description', null, array('placeholder' => 'Description','class' => 'form-control','style'=>'height:100px')) !!}
	                         @if ($errors->has('description'))
					            <span class="help-block">
					                <strong>{{ $errors->first('description') }}</strong>
					            </span>
					        @endif
	                    </div>
	                </div>
					
	                <div class="form-group{{ $errors->has('roles') ? ' has-error' : '' }} hide-list">
	                	<label class="col-md-2 control-label">Permission*</label>
						<div class="col-md-4">
							<ul>
								<li><label><input type="checkbox" class="minimal checkAllUsers"> Check All Users</label></li>
			                	@foreach(App\Permission::limit(7)->get() as $value)
			                		<ul>
			                			<li>
			                				<label title="{{$value->description}}">
			                					{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'minimal userCheck')) }} {{ $value->display_name }}
			                				</label>
			                			</li>
			                		</ul>
			                	@endforeach
			                	<li>
			                	@foreach(App\Permission::where('name', 'manag-roles')->get() as $value)
			                		<li>
			                			<label title="{{$value->description}}">
			                				{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'minimal')) }} {{ $value->display_name }}
			                			</label>
			                		</li>
			                	@endforeach
			                	
			                	@foreach(App\Permission::where('name', 'manag-menus')->get() as $value)
			                		<li>
			                			<label title="{{$value->description}}">
			                				{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'minimal')) }} {{ $value->display_name }}
			                			</label>
			                		</li>
			                	@endforeach

			                	@foreach(App\Permission::where('name', 'manag-settings')->get() as $value)
			                		<li>
			                			<label title="{{$value->description}}">
			                				{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'minimal')) }} {{ $value->display_name }}
			                			</label>
			                		</li>
			                	@endforeach

			                	@foreach(App\Permission::where('name', 'manag-contacts')->get() as $value)
			                		<li>
			                			<label title="{{$value->description}}">
			                				{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'minimal')) }} {{ $value->display_name }}
			                			</label>
			                		</li>
			                	@endforeach

			                	<li><label><input type="checkbox" class="minimal checkAllPosts"> Check All Posts</label></li>
			                	
			                	@foreach(App\Permission::where('name', 'like','%posts%')->get() as $value)
			                		<ul>
			                			<li>
			                				@if($value->name == 'manag-posts')
			                				<label title="{{$value->description}}">
			                					{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'minimal postCheck postManag')) }} {{ $value->display_name }}
			                				</label>
			                				@else
			                				<label title="{{$value->description}}">
			                					{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'minimal postCheck')) }} {{ $value->display_name }}
			                				</label>
			                				@endif
			                				
			                			</li>
			                		</ul>
			                	@endforeach

			                	<li><label><input type="checkbox" class="minimal checkAllPostCategories"> Check All Post Categories Management</label></li>
			                	@foreach(App\Permission::where('name', 'like','%categories%')->get() as $value)
			                		<ul>
				                		<li>
				                			<label title="{{$value->description}}">
				                				{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'minimal categoryPostCheck')) }} {{ $value->display_name }}
				                			</label>
				                		</li>
			                		</ul>
			                	@endforeach

			                	<li><label><input type="checkbox" class="minimal CheckAllPostLabels"> Check All Post Labels Management</label></li>
			                	@foreach(App\Permission::where('name', 'like','%labels%')->get() as $value)
			                		<ul>
				                		<li>
				                			<label title="{{$value->description}}">
				                				{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'minimal labelPostCheck')) }} {{ $value->display_name }}
				                			</label>
				                		</li>
			                		</ul>
			                	@endforeach

			                	<li><label><input type="checkbox" class="minimal CheckAllPages"> Check All Pages Management</label></li>
			                	@foreach(App\Permission::where('name', 'like','%pages%')->get() as $value)
			                		<ul>
				                		<li>
				                			<label title="{{$value->description}}">
				                				{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'minimal pagesCheck')) }} {{ $value->display_name }}
				                			</label>
				                		</li>
			                		</ul>
			                	@endforeach
			                </ul>
						</div>
						<div class="col-md-5">
							<ul>
								<li><label title="Role Owner tidak perlu check Kategori ini, karena secara default sudah bisa mengakses semua kategori."><input type="checkbox" class="minimal CheckAllCategories"> Check All Post Categories</label></li>
								@foreach(App\Permission::where('name', 'like','%category_post%')->get() as $value)
			                		<ul>
			                			<li>
			                				<label title="{{$value->description}}">
			                					{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'minimal categoryCheck')) }} {{ $value->display_name }}
			                				</label>
			                			</li>
			                		</ul>
			                	@endforeach
							</ul>
						</div>
					</div>

					<div class="note">
						<p><i><u>Catatan:</u><br> * : form wajib diisi</i></p>
					</div>
	                
	            </div>
	        </div>
	    </div>

	    <div class="box box-info">
            <div class="box-body">
                <!--div class="pull-left">
                    <a href="{{ url('sw-admin/users') }}" class="btn btn-danger btn-xs">Batal</a>
                </div-->

                <div class="pull-right">
                    <input type="submit" class="btn btn-success" value="Simpan" />
                </div>
                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->
		
		{!! Form::close() !!}

	</section>

@section('script')
<script src="{{asset('distLte/plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{asset('distLte/plugins/jQueryUI/jquery-ui.min.js')}}"></script>
<script>
$(document).ready(function () {
	$(".select2").select2();
	$('input[type="checkbox"].minimal').iCheck({
      	checkboxClass: 'icheckbox_minimal-blue'
    });
    
    /*$('.usercek').on('ifUnchecked', function (event) {
	    $('.checkAllUsers').iCheck('uncheck');
	});
	$('.usercek').on('ifChecked', function (event) {
	    if ($('.usercek').filter(':checked').length == $('.usercek').length) {
	        $('.checkAllUsers').iCheck('check');
	    }
	});*/
    $(".checkAllUsers").click(function(){
    	$('input:checkbox.usercek').not(this).prop('checked', this.checked);
    });

    var tooltips = $( "[title]" ).tooltip({
      position: {
        my: "left top",
        at: "right+5 top-5",
        collision: "none"
      }
    });
    
});
/* CheckAll jquery */
$(document).ready(function () {
	/* CheckAll for users */
	$('.checkAllUsers').on('ifChecked', function (event) {
	    $('.userCheck').iCheck('check');
	});
	$('.checkAllUsers').on('ifUnchecked', function (event) {
	    $('.userCheck').iCheck('uncheck');
	});
	/* CheckAll for Categories */
	$('.checkAllPostCategories').on('ifChecked', function (event) {
	    $('.categoryPostCheck').iCheck('check');
	});
	$('.checkAllPostCategories').on('ifUnchecked', function (event) {
	    $('.categoryPostCheck').iCheck('uncheck');
	});
	/* CheckAll for posts */
	$('.checkAllPosts').on('ifChecked', function (event) {
	    $('.postCheck').iCheck('check');
	});
	$('.checkAllPosts').on('ifUnchecked', function (event) {
	    $('.postCheck').iCheck('uncheck');
	});
	$('.postCheck').on('ifChecked', function (event) {
	    $('.postManag').iCheck('check');
	});
	/* CheckAllCategories */
	$('.CheckAllCategories').on('ifChecked', function (event) {
	    $('.categoryCheck').iCheck('check');
	});
	$('.CheckAllCategories').on('ifUnchecked', function (event) {
	    $('.categoryCheck').iCheck('uncheck');
	});
	/* CheckAllLabels */
	$('.CheckAllPostLabels').on('ifChecked', function (event) {
	    $('.labelPostCheck').iCheck('check');
	});
	$('.CheckAllPostLabels').on('ifUnchecked', function (event) {
	    $('.labelPostCheck').iCheck('uncheck');
	});
	/* CheckAllPages */
	$('.CheckAllPages').on('ifChecked', function (event) {
	    $('.pagesCheck').iCheck('check');
	});
	$('.CheckAllPages').on('ifUnchecked', function (event) {
	    $('.pagesCheck').iCheck('uncheck');
	});
});
</script>
@endsection

@endsection