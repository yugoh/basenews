@extends('appadmin')

@section('title', 'Roles Management')

@section('content')

@section('style')
  <style>
		.table-responsive td a {
			 margin-right: 15px;
		}
	</style>
@endsection

	<section class="content-header">
      <h1>
        Manajemen Roles
        <small><a href="{{url('sw-admin/roles/create')}} " class="btn btn-xs btn-default">Tambah Baru</a></small>
      </h1>
    </section>

    <section class="content">
    	<div class="row">
        <div class="col-xs-12">
            @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
            @endif
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>

              <div class="box-tools">
              	{!! Form::open(['action'=>['UserController@search'],'method' => 'GET','role'=>'search']) !!}
                <div class="input-group input-group-sm {!! $errors->has('s') ? 'has-error' : '' !!}" style="width: 200px;">
	                  <input type="text" name="s" class="form-control pull-right" placeholder="Ketik Nama Role">
	                  <div class="input-group-btn">
	                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
	                  </div>
                </div>
                {!! $errors->first('s', '<p class="help-block">:message</p>') !!}
                {!! Form::close() !!}
              </div>
            </div>

            <div class="box-body table-responsive no-padding">
              @if(!empty($s))<p style="margin-left: 10px"> <span style="font-size:14px"> @if($userall->count() == 0) <i>Tidak Ditemukan @else <i>Ditemukan <span style="color:red"> {{ $userall->count() }} </span> @endif pengguna dengan pencarian <span style="color:red">{{$s}}</span> </i></span> </p>@endif
              <table class="table table-hover">
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Deskripsi</th>
                  <th>Actions</th>
                </tr>
                @foreach($roles as $k => $v)
                <tr>
                  <td>{{++$i}} </td>
                  <td>{{$v->display_name}}</td>
                  <td> {{$v->description}}</td>
                  <td>
                  	<a href="{{ url('sw-admin/roles/'.$v->id.'/edit') }}" class="btn btn-xs btn-warning">
	                  		<i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i>
	                  </a>
                      @if($v->id != 1 && $v->id != 2)
                            {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $v->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs js-submit-confirm', 'data-toggle'=>'tooltip', 'data-placement'=>'top','Hapus Role','data-original-title'=>'Hapus Role']) !!}
                            {!! Form::close() !!}
	                  	    
                      @endif
                  </td>
                </tr>
                @endforeach
              </table>
              <p> {{ $roles->appends(compact('s'))->links() }} </p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>

@endsection
