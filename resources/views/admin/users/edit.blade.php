@extends('appadmin')

@section('title', 'Users Edit')

@section('content')

	<section class="content-header">
      <h1>
        Edit Pengguna
      </h1>
    </section>

    <section class="content">
		{!! Form::model($user, ['route' => ['users.update', $user->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'patch']) !!}
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Edit User</h3>
        
				<div class="box-body">
	                <div class="form-group{{ $errors->has('firstname') || $errors->has('lastname') ? ' has-error' : '' }}">
	                    <label for="firstname" class="col-md-2 control-label">Nama</label>
	                    <div class="col-md-4">
	                        <input class="form-control" placeholder="Firstname*" name="firstname" type="text" value="{{ $user->firstname }}" required autofocus>
	                         @if ($errors->has('firstname'))
					            <span class="help-block">
					                <strong>{{ $errors->first('firstname') }}</strong>
					            </span>
					        @endif
	                    </div>
	                    <div class="col-md-6">
	                        <input class="form-control" placeholder="Lastname" name="lastname" type="text" value="{{ $user->lastname }}">
	                         @if ($errors->has('lastname'))
					            <span class="help-block">
					                <strong>{{ $errors->first('lastname') }}</strong>
					            </span>
					        @endif
	                    </div>
	                </div><!--form control-->

	                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
	                    <label for="username" class="col-md-2 control-label">Username*</label>
	                    <div class="col-md-10">
	                        <input class="form-control" placeholder="Username" name="username" type="text" value="{{ $user->username }}" required>
	                         @if ($errors->has('username'))
					            <span class="help-block">
					                <strong>{{ $errors->first('username') }}</strong>
					            </span>
					        @endif
	                    </div>
	                </div><!--form control-->

	                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
	                    <label for="email" class="col-md-2 control-label">E-mail*</label>
	                    <div class="col-md-10">
	                        <input class="form-control" placeholder="E-mail" name="email" type="text" id="email" value="{{ $user->email }}" required>
	                         @if ($errors->has('email'))
					            <span class="help-block">
					                <strong>{{ $errors->first('email') }}</strong>
					            </span>
					        @endif
	                    </div>
	                </div><!--form control-->

	                @permission('banned-users')
	                <div class="form-group{{ $errors->has('suspended') ? ' has-error' : '' }}">
	                    <label class="col-md-2 control-label">Status*</label>
	                    <div class="col-md-1">
	                        <div class="radio">
			                    <label>
			                      <input type="radio" name="suspended" id="optionActive1" value="1" {{$user->suspended == 1? 'checked':''}}>
			                      Suspended
			                    </label>
			                  </div>
			                  <div class="radio">
			                    <label>
			                      <input type="radio" name="suspended" id="optionsActive2" value="0"  {{$user->suspended == 0? 'checked':''}}>
			                      Aktif
			                    </label>
			                  </div>
	                    </div>
	                </div><!--form control-->
	                @endpermission
					
					@permission('add-role')
	                <div class="form-group{{ $errors->has('roles') ? ' has-error' : '' }}">
	                	<label class="col-md-2 control-label">Hak Akses</label>
						<div class="col-md-10">
				            {!! Form::select('roles[]', $roles, $userRole, ['class'=>'form-control select2', 'multiple', 'data-placeholder'=>'Pilih Hak Akses']) !!}
							@if ($errors->has('roles'))
					            <span class="help-block">
					                <strong>{{ $errors->first('roles') }}</strong>
					            </span>
					        @endif
						</div>
					</div>
					@endpermission

					<div class="note">
						<p><i><u>Catatan:</u><br> * : form wajib diisi</i></p>
					</div>
	                
	            </div>
	        </div>
	    </div>

	    <div class="box box-info">
            <div class="box-body">
                <!--div class="pull-left">
                    <a href="{{ url('sw-admin/users') }}" class="btn btn-danger btn-xs">Batal</a>
                </div-->

                <div class="pull-right">
                    <input type="submit" class="btn btn-success" value="Simpan" />
                </div>
                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->
		
		{!! Form::close() !!}

	</section>

@section('script')
<script>
$(document).ready(function () {
	$(".select2").select2();
});
</script>
@endsection

@endsection