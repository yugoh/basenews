@extends('appadmin')

@section('title', 'Users Management')

@section('content')

@section('style')
  <style>
		.table-responsive td a {
			margin-right: 15px;
		}
	</style>
@endsection

	<section class="content-header">
      <h1>
        Manajemen Pengguna
        @permission('create-users')
        <small><a href="{{url('sw-admin/users/create')}} " class="btn btn-xs btn-default">Tambah Baru</a></small>
        @endpermission
      </h1>
    </section>

    <section class="content">
    	<div class="row">
        <div class="col-xs-12">
            @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
            @endif
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>

              <div class="box-tools">
              	{!! Form::open(['action'=>['UserController@search'],'method' => 'GET','role'=>'search']) !!}
                <div class="input-group input-group-sm {!! $errors->has('s') ? 'has-error' : '' !!}" style="width: 200px;">
	                  <input type="text" name="s" class="form-control pull-right" placeholder="Cari email/nama/username">
	                  <div class="input-group-btn">
	                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
	                  </div>
                </div>
                {!! $errors->first('s', '<p class="help-block">:message</p>') !!}
                {!! Form::close() !!}
              </div>
            </div>

            <div class="box-body table-responsive no-padding">
              @if(!empty($s))<p style="margin-left: 10px"> <span style="font-size:14px"> @if($userall->count() == 0) <i>Tidak Ditemukan @else <i>Ditemukan <span style="color:red"> {{ $userall->count() }} </span> @endif pengguna dengan pencarian <span style="color:red">{{$s}}</span> </i></span> </p>@endif
              <table class="table table-hover">
                <tr>
                  <th>No</th>
                  <th>Pengguna</th>
                  <th>Email</th>
                  <th>Status</th>
                  <th>Hak Akses</th>
                  <th>Created</th>
                  <th>Actions</th>
                </tr>
                @foreach($users as $user)
                <tr>
                  <td>{{++$i}}</td>
                  <td>{{$user->firstname}} {{$user->lastname}} </td>
                  <td>{{$user->email}}</td>
                  <td>
                    @if($user->suspended == 0)
                    <span class="label label-success">Aktif</span>
                    @else
                    <span class="label label-warning">Banned</span>
                    @endif
                  </td>
                  <td> 
                  		@if(!empty($user->roles))
                  			@foreach($user->roles as $v)
                  				<label class="label label-primary"><i class="fa fa-btn fa-tags"></i> {{ $v->display_name }}</label>
                  			@endforeach
                  		@endif
                  </td>
                  <?php $carbon = new Carbon\Carbon(); ?>
                  <td>{{ $carbon->createFromTimeStamp(strtotime($user->created_at))->diffForHumans() }}</td>
                  <td>
                  	  @permission('edit-users')
                  	  <a href="{{ url('sw-admin/users/'.$user->id.'/edit') }}" class="btn btn-xs btn-primary">
	                  		<i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title data-original-title="Edit"></i>
	                  </a>
	                  @endpermission
	                  @permission('change-password')
                      <a href="{{ url('sw-admin/users/'.$user->id.'/changepassword') }}" class="btn btn-xs btn-info">
                        <i class="fa fa-refresh" data-toggle="tooltip" data-placement="top" title data-original-title="Ubah Password"></i>
                      </a>
	                  @endpermission
                      @if($user->id != 1)
                          @permission('delete-users')
                            {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs js-submit-confirm', 'data-toggle'=>'tooltip', 'data-placement'=>'top','Hapus User','data-original-title'=>'Hapus User']) !!}
                            {!! Form::close() !!}
	                  	    @endpermission
                      @endif
                  </td>
                </tr>
                @endforeach
              </table>
              <p> {{ $users->appends(compact('s'))->links() }} </p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>

@endsection
