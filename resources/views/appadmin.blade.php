<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title', 'Admin | Dashboard')</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!--link rel="stylesheet" href="{{asset('distLte/css/AdminLTE.min.css')}} "-->
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  <link rel="stylesheet" href="{{ asset('css/admin-app.css') }}">
  @yield('css')
  <link rel="stylesheet" href="{{ asset('css/admin-style.css') }}">
  @yield('style')

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-purple-light sidebar-mini">
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="{{url('sw-admin/dashboard')}}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>dm</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Administrator</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">{{$countnotifcontact}}</span>
            </a>
            <ul class="dropdown-menu message_notif">
              <li class="header">{{$countnotifcontact}} pesan belum terbaca.</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <?php $carbon = new Carbon\Carbon; ?>
                  @foreach($notifcontacts as $contact)
                  <li><!-- start message -->
                    <a href="{{url('sw-admin/contacts/'.$contact->id)}}">
                      <h4>
                        <span class="notif_name">{{$contact->name}}</span>
                        <small><i class="fa fa-clock-o"></i> {{ $carbon->createFromTimeStamp(strtotime($contact->created_at))->diffForHumans() }}</small>
                      </h4>
                      <p>{{$contact->title}}</p>
                    </a>
                  </li>
                  @endforeach
                  <!-- end message -->
                </ul>
              </li>
              <li class="footer"><a href="{{url('sw-admin/contacts')}}">Lihat Semua Pesan</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <!--li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-danger">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                      page and may cause design problems
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 5 new members joined
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> You changed your username
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li-->
          <!-- Tasks: style can be found in dropdown.less -->
          
          <li class="dropdown">
            <a href="{{url('/')}}">Front Pages</a>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="{{ url('/sw-admin/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Log Out</a>
            <form id="logout-form" action="{{ url('/sw-admin/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">NAVIGASI UTAMA</li>
        <li class="{{ Request::is('sw-admin/dashboard')?'active':'' }}{{ Request::is('sw-admin')?'active':'' }}">
          <a href="{{url('sw-admin/dashboard')}}">
            <i class="fa fa-dashboard"></i> <span>Dasbor</span>
          </a>
        </li>
        @permission('manag-posts')
        <li class="treeview {{ Request::is('sw-admin/posts')?'active':'' }}{{ Request::is('sw-admin/posts/create')?'active':'' }}{{ Request::is('sw-admin/categories')?'active':'' }}{{ Request::is('sw-admin/labels')?'active':'' }}">
          <a href="#">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Posting</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li {{ Request::is('sw-admin/posts')?'class=active':'' }}><a href="{{url('sw-admin/posts')}} "><i class="fa fa-circle-o"></i> Semua Postingan</a></li>
            @permission('create-posts')
            <li {{ Request::is('sw-admin/posts/create')?'class=active':'' }}><a href="{{url('sw-admin/posts/create')}} "><i class="fa fa-circle-o"></i> Tambah Posting Baru</a></li>
            @endpermission
            @permission('list-categories')
            <li {{ Request::is('sw-admin/categories')?'class=active':'' }}><a href="{{url('sw-admin/categories')}}"><i class="fa fa-circle-o" aria-hidden="true"></i> <span> Kategori </span></a></li>
            @endpermission
            @permission('list-labels')
            <li {{ Request::is('sw-admin/labels')?'class=active':'' }}><a href="{{url('sw-admin/labels')}}"><i class="fa fa-circle-o" aria-hidden="true"></i> <span> Label </span></a></li>
            @endpermission
          </ul>
        </li>
        @endpermission
        @permission('list-pages')
        <li class="treeview {{ Request::is('sw-admin/pages')?'active':'' }}{{ Request::is('sw-admin/pages/create')?'active':'' }}">
          <a href="#">
            <i class="fa fa-file-text-o" aria-hidden="true"></i> <span>Halaman</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li {{ Request::is('sw-admin/pages')?'class=active':'' }}><a href="{{url('sw-admin/pages')}} "><i class="fa fa-circle-o"></i> Semua Halaman</a></li>
            @permission('create-pages')
            <li {{ Request::is('sw-admin/pages/create')?'class=active':'' }}><a href="{{url('sw-admin/pages/create')}} "><i class="fa fa-circle-o"></i> Tambah Halaman Baru</a></li>
            @endpermission
          </ul>
        </li>
        @endpermission
        <li class="treeview {{ Request::is('sw-admin/users')?'active':'' }} {{ Request::is('sw-admin/users/search')?'active':'' }} {{ Request::is('sw-admin/users/create')?'active':'' }}{{ Request::is('sw-admin/users/'.Auth::user()->id.'/edit-profile')?'active':'' }}{{ Request::is('sw-admin/users/'.Auth::user()->id.'/changepasswordprofile')?'active':'' }}">
          <a href="#">
            <i class="fa fa-address-card-o" aria-hidden="true"></i> <span>Pengguna</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            @permission('list-users')
            <li {{ Request::is('sw-admin/users')?'class=active':'' }} {{ Request::is('sw-admin/users/search')?'class=active':'' }}><a href="{{url('sw-admin/users')}} "><i class="fa fa-circle-o"></i> Semua Pengguna</a></li>
            @endpermission
            @permission('create-users')
            <li {{ Request::is('sw-admin/users/create')?'class=active':'' }}><a href="{{url('sw-admin/users/create')}} "><i class="fa fa-circle-o"></i> Tambah Pengguna</a></li>
            @endpermission
            <li {{ Request::is('sw-admin/users/'.Auth::user()->id.'/edit-profile')?'class=active':'' }}><a href="{{ url('sw-admin/users/'.Auth::user()->id.'/edit-profile') }}"><i class="fa fa-circle-o"></i> Profile </a></li>
            <li {{ Request::is('sw-admin/users/'.Auth::user()->id.'/changepasswordprofile')?'class=active':'' }}><a href="{{ url('sw-admin/users/'.Auth::user()->id.'/changepasswordprofile') }}"><i class="fa fa-circle-o"></i> Ganti Password </a></li>
          </ul>
        </li>
        @permission('manag-roles')
        <li class="{{ Request::is('sw-admin/roles')?'active':'' }}{{ Request::is('sw-admin/roles/create')?'active':'' }}">
          <a href="{{url('sw-admin/roles')}}">
            <i class="fa fa-user-circle"></i> <span>Roles</span>
          </a>
        </li>
        @endpermission
        @permission('manag-menus')
        <li class="treeview {{ Request::is('sw-admin/menu')?'active':'' }}{{ Request::is('sw-admin/secondmenu')?'active':'' }}">
          <a href="#">
            <i class="fa fa-caret-square-o-down" aria-hidden="true"></i> <span>Menu</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li {{ Request::is('sw-admin/menu')?'class=active':'' }}><a href="{{url('sw-admin/menu')}} "><i class="fa fa-circle-o"></i> Menu Utama</a></li>
          </ul>
        </li>
        @endpermission
        @permission('manag-settings')
        <li class="treeview {{ Request::is('sw-admin/settings')?'active':'' }}">
          <a href="{{url('sw-admin/settings')}}">
            <i class="fa fa-gear" aria-hidden="true"></i> <span>Pengaturan</span>
          </a>
        </li>
        @endpermission
        @permission('manag-contacts')
        <li class="treeview {{ Request::is('sw-admin/contacts')?'active':'' }}">
          <a href="{{url('sw-admin/contacts')}}">
            <i class="fa fa-envelope-o" aria-hidden="true"></i> <span>Pesan Kontak</span>
          </a>
        </li>
        @endpermission
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

  @yield('content')

  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2017 <a href="http://www.samarindaweb.com">samarindaweb.com</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->

    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->


<script src="{{ asset('js/admin-all.js') }}"></script>

@yield('js')

@yield('script')

</body>
</html>
