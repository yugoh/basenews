<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Route::get('sw-admin', function() {
	return view('admin.index');
});*/

//Auth::routes();
Route::get('sw-admin/login', 'Auth\LoginController@showLoginForm');
Route::post('sw-admin/login', 'Auth\LoginController@login');
Route::post('sw-admin/logout', 'Auth\LoginController@logout');

Route::group(['middleware' => ['auth', 'role:owner|admin']], function () {
	Route::get('/sw-admin/dashboard', ['as' => 'dashboard', 'uses' => 'HomeController@index']);
	Route::get('/sw-admin', function (){ return redirect('/sw-admin/dashboard'); });
	/* Route for Categories */
	Route::get('sw-admin/categories', ['as'=>'categories.index','uses' => 'CategoryController@index','middleware'=>['permission:list-categories']]);
	Route::post('sw-admin/categories/create', ['as'=>'categories.store','uses' => 'CategoryController@store','middleware'=>['permission:create-categories']]);
	Route::get('sw-admin/categories/{id}/edit', ['as'=>'categories.edit','uses' => 'CategoryController@edit','middleware'=>['permission:edit-categories']]);
	Route::patch('sw-admin/categories/{id}', ['as'=>'categories.update','uses' => 'CategoryController@update','middleware'=>['permission:edit-categories']]);
	Route::delete('sw-admin/categories/{id}', ['as'=>'categories.destroy','uses'=>'CategoryController@destroy','middleware'=>['permission:delete-categories']]);
	/* Route for Users */
	Route::get('sw-admin/users', ['as'=>'users.index','uses' => 'UserController@index','middleware'=>['permission:list-users']]);
	Route::get('sw-admin/users/create', ['as'=>'users.create','uses' => 'UserController@create','middleware'=>['permission:create-users']]);
	Route::post('sw-admin/users/create', ['as'=>'users.store','uses' => 'UserController@store','middleware'=>['permission:create-users']]);
	Route::get('sw-admin/users/{id}/edit-profile', ['as'=>'users.editprofile','uses' => 'UserController@editProfile']);
	Route::get('sw-admin/users/{id}/edit', ['as'=>'users.edit','uses' => 'UserController@edit','middleware'=>['permission:edit-users']]);
	Route::patch('sw-admin/users/{id}', ['as'=>'users.update','uses' => 'UserController@update']);
	Route::get('sw-admin/users/search', ['uses'=>'UserController@search', 'middleware'=>['permission:list-users']]);
	Route::get('sw-admin/users/{id}/changepassword', ['uses'=>'UserController@changepassword','middleware'=>['permission:change-password']]);
	Route::get('sw-admin/users/{id}/changepasswordprofile', ['uses'=>'UserController@changePasswordProfile']);
	Route::post('sw-admin/users/{id}/changepassword', ['as'=>'update-password','uses'=>'UserController@updatePassword']);
	Route::delete('sw-admin/users/{id}', ['as'=>'users.destroy','uses'=>'UserController@destroy','middleware'=>['permission:delete-users']]);

	
	Route::resource('sw-admin/roles', 'RoleController');
	
	//Route Posts
	Route::get('sw-admin/posts', ['as'=>'posts.index', 'uses'=>'PostController@index', 'middleware'=>['permission:manag-posts,list-posts']]);
	Route::get('sw-admin/posts/create', ['as'=>'posts.create', 'uses'=>'PostController@create', 'middleware'=>['permission:manag-posts,create-posts']]);
	Route::post('sw-admin/posts/create', ['as'=>'posts.store', 'uses'=>'PostController@store', 'middleware'=>['permission:manag-posts,create-posts']]);
	Route::get('sw-admin/posts/{id}/edit', ['as'=>'posts.edit', 'uses'=>'PostController@edit', 'middleware'=>['permission:manag-posts']]);
	Route::patch('sw-admin/posts/{id}/', ['as'=>'posts.update', 'uses'=>'PostController@update', 'middleware'=>['permission:manag-posts']]);
	Route::delete('sw-admin/posts/{id}/', ['as'=>'posts.destroy', 'uses'=>'PostController@destroy', 'middleware'=>['permission:manag-posts,delete-posts']]);
	Route::get('sw-admin/posts/search', ['as'=>'posts.search','uses'=>'PostController@search', 'middleware'=>['permission:manag-posts,list-posts']]);

	//Route labels
	Route::get('sw-admin/labels', ['as'=>'labels.index','uses' => 'LabelController@index','middleware'=>['permission:list-labels']]);
	Route::post('sw-admin/labels/create', ['as'=>'labels.store','uses' => 'LabelController@store','middleware'=>['permission:create-labels']]);
	Route::get('sw-admin/labels/{id}/edit', ['as'=>'labels.edit','uses' => 'LabelController@edit','middleware'=>['permission:edit-labels']]);
	Route::patch('sw-admin/labels/{id}', ['as'=>'labels.update','uses' => 'LabelController@update','middleware'=>['permission:edit-labels']]);
	Route::delete('sw-admin/labels/{id}', ['as'=>'labels.destroy','uses'=>'LabelController@destroy','middleware'=>['permission:delete-labels']]);

	Route::get('sw-admin/ajaxcategory', ['uses'=>'PostController@ajaxcategory','middleware'=>['permission:create-categories']]);
	Route::get('sw-admin/ajaxlabel', ['uses'=>'PostController@ajaxlabel','middleware'=>['permission:create-labels']]);

	/* Pages */
	Route::get('sw-admin/pages', ['as'=>'pages.index','uses' => 'PageController@index','middleware'=>['permission:list-pages']]);
	Route::get('sw-admin/pages/create', ['as'=>'pages.create','uses' => 'PageController@create','middleware'=>['permission:create-pages']]);
	Route::post('sw-admin/pages/create', ['as'=>'pages.store','uses' => 'PageController@store','middleware'=>['permission:create-pages']]);
	Route::get('sw-admin/pages/{id}/edit', ['as'=>'pages.edit','uses' => 'PageController@edit','middleware'=>['permission:edit-pages']]);
	Route::patch('sw-admin/pages/{id}', ['as'=>'pages.update','uses' => 'PageController@update','middleware'=>['permission:edit-pages']]);
	Route::delete('sw-admin/pages/{id}', ['as'=>'pages.destroy','uses'=>'PageController@destroy','middleware'=>['permission:delete-pages']]);
	Route::get('sw-admin/search/pages', ['as'=>'search.pages','uses'=>'PageController@search']);

	/* Menus */
	Route::get('sw-admin/menu', 'MenuController@top');
	Route::post('sw-admin/topmenu/update', 'MenuController@updateTop');
	Route::get('sw-admin/menu/ajaxpage', 'MenuController@ajaxPage');
	Route::get('sw-admin/menu/ajaxlink', 'MenuController@ajaxLink');
	Route::get('sw-admin/menu/ajaxcategory', 'MenuController@ajaxCategory');
	Route::get('sw-admin/menu/delete', 'MenuController@delete');

	/* Options */
	Route::get('sw-admin/settings', 'OptionController@setGlobal');
	Route::post('sw-admin/save-global', ['as'=>'restore.global','uses'=>'OptionController@restoreGlobal']);
	Route::post('sw-admin/save-setmail', ['as'=>'restore.setmail','uses'=>'OptionController@restoreMail']);

	/* Contacts */
	Route::resource('sw-admin/contacts', 'ContactController');
	Route::get('sw-admin/search/contacts', ['as'=>'search.contacts','uses'=>'ContactController@search']);

});

